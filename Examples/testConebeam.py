#!/usr/bin/python
'''Run a single Projection and Backprojection'''
import os.path
import sys
import numpy as np
import PyCACalebExtras.SetBackend
import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd
import PyCA.Core as ca
import PyCA.Common as common

# cc.SelectGPU()
plt = PyCACalebExtras.SetBackend.SetBackend()
plt.ion()
plt.close('all')
# relative import of ConbeamProjection - add module to path
_cbmain = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
_cbdir = os.path.join(_cbmain, 'ConebeamProjection')
sys.path.append(_cbdir)

import ConebeamProjection as cb

import AppUtils.Config as cfg

# volgrid = ca.GridInfo(ca.Vec3Di(100, 100, 100))
volgrid = ca.GridInfo(ca.Vec3Di(50, 50, 50))
vol = ca.Image3D(volgrid, ca.MEM_DEVICE)
volbpj = ca.Image3D(volgrid, ca.MEM_DEVICE)
vol.setOrigin(ca.Vec3Df(-25, -25, -25))
volbpj.setOrigin(ca.Vec3Df(-25, -25, -25))

imgrid = ca.GridInfo(ca.Vec3Di(100, 100, 1))
# imgrid = ca.GridInfo(ca.Vec3Di(50, 50, 1))
im = ca.Image3D(imgrid, ca.MEM_DEVICE)
im.setOrigin(ca.Vec3Df(-50, -50, 0))
# im.setOrigin(ca.Vec3Df(-25, -25, 0))

cc.CreateSphere(vol, 19)
cc.AddSphere(vol, 17, val=-1.0)
cc.AddSphere(vol, 5, center=[-8, 5, 2], val=0.8)
cc.AddSphere(vol, 5, center=[8, 5, 2], val=0.8)

cd.DispImage(vol, colorbar=True, title='Volume', axis='cart')

# project from +x-axis
cf = cfg.SpecToConfig(cb.ConebeamProjectionConfigSpecs)
cf.cam_rot = np.matrix([[0, 0, -1],
                        [1, 0, 0],
                        [0, 1, 0]])
cf.cam_trans = [0, 0, -250]
cf.sid = 500

cb.ConebeamProject(im, vol, cf)
cd.DispImage(im, colorbar=True, title='from +x', axis='cart')

# project from +y-axis
cf.cam_rot = np.matrix([[-1, 0, 0],
                        [0, 0, -1],
                        [0, 1, 0]])
cf.cam_trans = [0, 0, -250]
cf.sid = 500

cb.ConebeamProject(im, vol, cf)
cd.DispImage(im, colorbar=True, title='from +y', axis='cart')

ca.SetMem(volbpj, 0.0)
cb.ConebeamBackproject(volbpj, im, cf)
print ca.MinMax(volbpj)
cd.DispImage(volbpj, colorbar=True, title='voxel backprojection')

ca.SetMem(volbpj, 0.0)
cb.ConebeamBackprojectSplat(volbpj, im, cf, 0.2)
print ca.MinMax(volbpj)
cd.DispImage(volbpj, colorbar=True, title='splat backprojection')


# project from +z axis
cf.cam_rot = np.matrix([[-1, 0, 0],
                        [0, 1, 0],
                        [0, 0, 1]])
cf.cam_trans = [0, 0, -250]
cf.sid = 500
cb.ConebeamProject(im, vol, cf)
print ca.MinMax(im)
cd.DispImage(im, colorbar=True, title='from +z', axis='cart')
