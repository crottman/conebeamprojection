import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule

import numpy as np
np.set_printoptions(linewidth = 100)

# a = np.random.randn(32,32)
# a = np.array(range(32*32)).reshape(32, 32).astype(np.float32)

N = 8
a = np.array(range(N*N)).reshape(N, N).astype(np.float32)
aorig = a.copy()
asum = np.sum(a)
print a

mod = SourceModule("""

  #include <stdio.h>
  __global__ void doublify(float *a)
  {
    int idx = threadIdx.y*blockDim.x+threadIdx.x;
    a[idx] *= 2;
  }
  """)

func = mod.get_function("doublify")
Imblock = (8, 8, 1)
Imgrid = (1, 1, 1)
func(cuda.InOut(a), block=Imblock, grid=Imgrid)


print a

# print a/aorig

assert(asum*2 == np.sum(a))
print "PyCUDA Test passed!"
