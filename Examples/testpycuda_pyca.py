'''tests running some simple PyCUDA functions on pyca objects'''

import pycuda.driver as cuda
import pycuda.autoinit
from pycuda.compiler import SourceModule

import numpy as np

import matplotlib.pyplot as plt
plt.ion()   # tell it to use interactive mode -- see results immediately

import PyCA.Core as ca
# import PyCACalebExtras.Common as cc
# import PyCACalebExtras.Display as cd

plt.close('all')
# imagedir = '/home/sci/crottman/GE/DSA/PNG/'

# vsm = cc.GenRandomVDef(cc.MakeGrid(64, 64))
vsm = ca.Field3D(ca.GridInfo(ca.Vec3Di(64,64,1)), ca.MEM_DEVICE)
val1 = -1
val2 = 2
val3 = 3
ca.SetMem(vsm, ca.Vec3Df(val1, val2, val3))
I1 = ca.Image3D(vsm.grid(), ca.MEM_DEVICE)
I2 = ca.Image3D(vsm.grid(), ca.MEM_DEVICE)
I3 = ca.Image3D(vsm.grid(), ca.MEM_DEVICE)

ca.Copy(I1, vsm, 0)
ca.Copy(I2, vsm, 1)

p1 = np.uint64(I1.rawptr())
p2 = np.uint64(I2.rawptr())
p3 = np.uint64(I3.rawptr())


mod = SourceModule("""
  #include <stdio.h>

  __global__ void TRIPLE_I(float* a)
  {
    int blockId = blockIdx.x
                  + blockIdx.y * gridDim.x
                  + gridDim.x * gridDim.y * blockIdx.z;
    int threadId = blockId * (blockDim.x * blockDim.y * blockDim.z)
                  + (threadIdx.z * (blockDim.x * blockDim.y))
                  + (threadIdx.y * blockDim.x)
                  + threadIdx.x;
    //printf("I am %d.%d, %d.%d\\n", blockIdx.x, blockIdx.y, threadIdx.x, threadIdx.y);
    a[threadId] *= 3;
  }

  __global__ void SUB_I(float *a, float *b)
  {
    int blockId = blockIdx.x
                  + blockIdx.y * gridDim.x
                  + gridDim.x * gridDim.y * blockIdx.z;
    int idx     = blockId * (blockDim.x * blockDim.y * blockDim.z)
                  + (threadIdx.z * (blockDim.x * blockDim.y))
                  + (threadIdx.y * blockDim.x)
                  + threadIdx.x;

    a[idx] = a[idx]-b[idx];
  }

  __global__ void SUB(float *a, float *b, float *c)
  {
    int blockId = blockIdx.x
                  + blockIdx.y * gridDim.x
                  + gridDim.x * gridDim.y * blockIdx.z;
    int idx     = blockId * (blockDim.x * blockDim.y * blockDim.z)
                  + (threadIdx.z * (blockDim.x * blockDim.y))
                  + (threadIdx.y * blockDim.x)
                  + threadIdx.x;

    a[idx] = b[idx]-c[idx];
  }

  __global__ void MULC_I(float* a, float b)
  {
    int blockId = blockIdx.x
                  + blockIdx.y * gridDim.x
                  + gridDim.x * gridDim.y * blockIdx.z;
    int idx     = blockId * (blockDim.x * blockDim.y * blockDim.z)
                  + (threadIdx.z * (blockDim.x * blockDim.y))
                  + (threadIdx.y * blockDim.x)
                  + threadIdx.x;

    a[idx] *= b;
  }

  __global__ void TEST_TYPES(float a, unsigned int b, bool c)
  {
    int blockId = blockIdx.x
                  + blockIdx.y * gridDim.x
                  + gridDim.x * gridDim.y * blockIdx.z;
    int idx     = blockId * (blockDim.x * blockDim.y * blockDim.z)
                  + (threadIdx.z * (blockDim.x * blockDim.y))
                  + (threadIdx.y * blockDim.x)
                  + threadIdx.x;
    if(idx==0){
      printf("float value is %f\\n", a);
      printf("unsigned int value is %d\\n", b);
    }
  }


  """)
Imblock = (8, 8, 1)
Imgrid = (int(np.ceil(float(I1.size().x)/Imblock[0])),
          int(np.ceil(float(I1.size().y)/Imblock[1])),
          int(np.ceil(float(I1.size().z)/Imblock[2])))


func1 = mod.get_function("TRIPLE_I")
func2 = mod.get_function("SUB_I")
func3 = mod.get_function("SUB")
func4 = mod.get_function("MULC_I")
func5 = mod.get_function("TEST_TYPES")

func5(np.float32(3.14), np.uint32(32), np.uint32(True), block=Imblock, grid=Imgrid)

# I3 = I2-I1 = (2 - -1) = 3
# func3(cuda.InOut(array3), cuda.In(array2), cuda.In(array1), block=Imblock, grid=Imgrid)
func3(p3, p2, p1, block=Imblock, grid=Imgrid)
# cd.DispImage(I3, colorbar=True)
print ca.MinMax(I3), val2-val1
assert ca.Min(I3) == ca.Max(I3)
np.testing.assert_approx_equal(ca.Min(I3), val2-val1)

# I1 = I1-I2 = -1 - 2 = -3
# func2(cuda.InOut(array1), cuda.In(array2), block=Imblock, grid=Imgrid)
func2(p1, p2, block=Imblock, grid=Imgrid)
# cd.DispImage(I1, colorbar=True)
print ca.MinMax(I1), val1-val2
assert ca.Min(I1) == ca.Max(I1)
np.testing.assert_approx_equal(ca.Min(I1), val1-val2)

# I2 *= 3 = 2*3 = 6
# func1(cuda.InOut(array1), block=Imblock, grid=Imgrid)
func1(p2, block=Imblock, grid=Imgrid)
# cd.DispImage(I2, colorbar=True)
print ca.MinMax(I2), val2*3
val2 *= 3
assert ca.Min(I2) == ca.Max(I2)
np.testing.assert_approx_equal(ca.Min(I2), val2)

# I2 /= 2 = 6/2 = 3
# divide image by 2
func4(p2, np.float32(0.5), block=Imblock, grid=Imgrid)
# cd.DispImage(I2, colorbar=True)
print ca.MinMax(I2), val2/2.0
assert ca.Min(I2) == ca.Max(I2)
np.testing.assert_approx_equal(ca.Min(I2), val2/2.0)

print "PyCUDA-PyCA Test passed!"
