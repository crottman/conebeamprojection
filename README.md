#ConebeamProjection:  GPU implemented cone-beam projection and backprojection modules for Python


This python module includes PyCUDA-implemented cone-beam projection and backprojection
operations that interface with PyCA data structures


##Required Dependencies

- [PyCA](https://bitbucket.org/scicompanat/pyca) 0.01+
- [PyCUDA](http://wiki.tiker.net/PyCuda/Installation/Linux) 2013.1.1+
- Cuda 5+
- [AppUtils](https://bitbucket.org/jhinkle/apputils) 0.01

##Optional Dependencies
- [PyCACalebExtras](https://bitbucket.org/crottman/pycacalebextras) (used for some plotting)

##Installation

Build PyCA, and then add the location of AppUtils and
ConebeamProjection to your `$PYTHON_PATH`.  No compliation is
necessary for AppUtils or ConebeamProjection

##Usage

###Coordinate Systems and Geometric Parameters

The geometric parameters of a single projection/backprojection in the cone-beam
system can be described by 3D translation T, a 3D rotation matrix R, a 2D piercing point, and a 1D SID.

World coordinates are defined by the volume Image3D, and the Image3D's GridInfo.
This GridInfo contains the origin and spacing. Given an index coordinate xi = (xi, yi, zi) the real world coordinates x = (x, y, z) are:

x = xi * spacing + origin

Projection coordinates u = (u, v) are defined by the 2D projection image (also an Image3D with an origin and spacing). The transformation between real projection coordinates and index projection coordinates are similarly defined with the origin and spacing.

Camera coordinates linke these two systems. In camera coordinates, the X-ray source is at xc = (0, 0, 0) and the positive z axis points at the piercing point. Given the piercing point (u0, v0) and the SID, the point (u,v) in projection coordinates is the point xc = (u-u0, v-v0, SID) in camera coordinates.

The camera coordinates are related to the world coord by a rigid transformation (rotation + translation).
A point p in world coordinates is equivalently defined by the point p' in camera coordinates by:

p = R(p' + T).

####Example

Suppose we want to simulate a projection system where the X-ray source is 250 mm in the +x axis
and the piercing point is 250 mm in the -x axis. Here is how this is done.

These geometric parameters are defined by a config object:

    import AppUtils.Config
    import ConebeamProjection as cb
    import numpy as np

    cf = AppUtils.Config.SpecToConfig(cb.ConebeamProjectionConfigSpecs)

    cf.cam_rot = np.matrix([[0, 0, -1], # 3D Rotation
                            [1, 0, 0],
                            [0, 1, 0]])
    cf.cam_trans = [250, 0, 0]     # 3D Translation
    cf.sid = 500
    cf.piercing_point = [0, 0]

###Projection Operators

####Trilinear interpolation

A projection operator is a series of line integrals through the 3D volume from the point source to individual pixels.
There are two implemented projections operators based on ray-tracing. The first is a fixed step-size line integral with trilinear interpolation at the voxel interiors. Given a geometry config object `cf`, an Image3D volume `vol`, and and projection Image3D `proj`, this is called by:

    cb.ConebeamProject(proj, vol, cf, ds=None, onesOnly=False)

The parameter `ds` is the step size. If left to None, a smart step size will be chosen.
If the parameter `onesOnly` is set to `True`, it will integrate through a volume of ones (given the volume size of `vol`).

####Bilinear interpolation
A second projection operation is similar to the first, but does a bilinear at the (most perpendicular) voxel faces.
Bilinear interpolation is faster, so this projection method is around 50% faster than the first.
This is called similar to the first, although there is no fixed step size:

    cb.ConebeamProjectFast(proj, vol, cf, onesOnly=False)

###Backprojection Operators

There are two backprojection operators. The first (most commonly used) is a voxel-based backprojection which bilinearly interpolates the projection grid for each voxel in the 3D volume. It is called by:

    cb.ConebeamBackProject(vol, proj, cf, mask=None, addResult=True, onesOnly=False)

`vol`, `proj`, and `cf` are the same as before. The `mask` allows a binary masking of the volume so that the updates are only applied in certain voxels. The `addResult` parameter will add backprojection to the current value of the volume.
In this case, the `onesOnly` backprojects a 2D image of ones instead of using the pixel values of `proj`.

The second backprojection operator is the numerical adjoint of the Trilinear projection operator.
Instead of interpolating the projection grid, it splats each projection pixel onto the volume image.
This is significantly slower, but acts in the same way:

    cb.ConebeamBackprojectSplat(vol, proj, cf, ds=None, mask=None, addResult=True, onesOnly=False)

This has the same step-size parameter `ds` as the trilinear projection operator.

###Gradient Operations

There are two functions that determine the mismatch functional gradient with respect two the extrinsic and intrinsic parameters.

The first is the gradient with respect to the SSE between the forward projection and the volume:

    ConebeamPoseGradient(proj, vol, cf, ds=None, gradmask=None, mask=None)

Again, `gradmask` is a binary volume the same size as `vol` and `mask` is a binary image the same size as `proj`.

The second is the gradient with respect to the local normalized cross-correlation (NCC). Currently, the kernel size parameter is fixed:

    ConebeamPoseGradientNCC(proj, vol, cf, gradmask=None, mask=None, calcParamIndex=None)

calcParamIndex is a 10-length list of bools. If one of the gradients doesn't need to be calculated, it can be set to 0.

###Other functions

Objective functions:

    ConebeamNCC(projdata, vol, cf, gradmask=None, mask=None, returnImage=False)
    ConebeamSumSquaredError(proj, vol, cf)
    ConebeamSumSquaredErrorFast(proj, vol, cf)

TV volume derivative. This is the TV derivative with respect to the image.
The stencil from Panin, Zeng, Gullberg (1999) is used.
This is useful for TV-OSEM.

    ComputeTVDerivative(d_vol, vol, eps=1e-5, cbmod=None)
