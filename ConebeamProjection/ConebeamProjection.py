'''Functions for Cone-beam Projection and Backprojection.

This module uses PyCUDA to load ConebeamProjectionKernels.cu perform
the Projection and Backprojection operations

The required parameters for a projection/backprojection is the camera
rotation matrix, the camera translation, and the SID (source to image
distance).

The transformation from camera to world coordinates is

# (x, y, z) = R * (x', y', z') + T  #OldCS
(x, y, z) = R * ((x', y', z') + T)  #NewCS
          = R*(x', y', z') + R*T    #NewCS

where x, y, and z are the world coordinates and
x', y', and z' are the camera coordinates

e.g. = if [x', y', z'] = [0, 0, 0] (x-ray source in camera
coordinates) then the x-ray source is at point T in world coordinates

where the piercing point is located at R*[0, 0, SID]'+ T in world
coordinates, or [0, 0, SID] in camera coordinates. Here R is the
rotation matrix that rotates the camera w.r.t world coordinates and T
is the camera translation. The source is always at point T in world
coordinates and the Origin in camera coordinates. The point of the
cone-beam is always in the origin, and shoots x-rays centered around
the z' axis.

SID and piercing point are both expressed in world units (usually mm).
Each projection has a pixel spacing (Sx, Sy) (mm/pix) and a image
origin (Ox, Oy) (mm) Therefore, in camera coordinates, the piercing
point is always located at (x', y', z') = (0, 0, SID). In the
projection "world-unit" coordinates, the piercing point is located at
(PPx, PPy) with respect to the image origin. We therefore have the
following relationships between camera coordinates and pixels (u, v):

(x', y', z') = (u*Sx + Ox - PPx, v*Sy + Oy - PPy, SID)
and
(u, v) = ((x'- Ox + PPx)/Sx, (y'- Oy + PPy)/Sy)

'''

import pycuda.driver as cuda
from pycuda.compiler import SourceModule

import os
import numpy as np

import PyCA.Core as ca

from AppUtils import Config

# Synchronize PyCUDA calls - useful for profiling
sync = True

Imblock = (8, 8, 1)


def _valspecconebeam(cf):
    '''ConebeamProjection config validation'''

    # Camera Rotation
    # make sure cam_rot is a 3x3 rotation matrix/array
    if np.shape(cf.cam_rot) != (3, 3):
        raise Exception('The camera rotation matrix (cam_rot) must be 3x3')
    # if abs(1-abs(np.linalg.det(cf.cam_rot))) > 1.e-9:
    if abs(1-abs(np.linalg.det(cf.cam_rot))) > 1.e-3:
        raise Exception('The camera rotation matrix must have det 1 or -1')
    # if np.linalg.norm(np.linalg.inv(cf.cam_rot) -
    #                   cf.cam_rot.transpose()) > 1.e-9:
    if np.linalg.norm(np.linalg.inv(cf.cam_rot) -
                      cf.cam_rot.transpose()) > 1.e-3:
        raise Exception('The camera rotation matrix is not a true rotation '
                        'matrix: R^T != R^-1')

    # Translation
    if len(cf.cam_trans) != 3:
        raise Exception('The camera translation vector ' +
                        '(cam_trans) must be of length 3')
    if np.isnan(cf.cam_trans).any():
        raise Exception('Camera translation can\'t be NAN')
    if np.isinf(cf.cam_trans).any():
        raise Exception('Camera translation can\'t be inf')

    # make sure translation, PP, SID can handle float arithmetic
    cf.cam_trans = np.array(cf.cam_trans, np.float64, copy=False)
    cf.cam_rot = np.array(cf.cam_rot, np.float64, copy=False)
    cf.piercing_point = np.array(cf.piercing_point, np.float64, copy=False)


ConebeamProjectionConfigSpecs = {
    'cam_rot': Config.Param(required=True,
                            comment="Camera rotation from world coordinates. "
                            "The location of the piercing point starts "
                            "at (0, 0, sid) in camera coords"),
    'cam_trans': Config.Param(required=True,
                              comment="Source translation from world "
                              "coordinates."),
    'sid': Config.Param(required=True,
                        comment="Source to Image Distance"),
    'piercing_point': Config.Param(required=True,
                                   comment="(u0, v0) - the piercing point "
                                   "offset from the projection origin.",
                                   default=np.zeros(2)),
    '_validation_hook': _valspecconebeam
}
# Compile and load CUDA file
_cbdir = os.path.dirname(__file__)
_f = open(os.path.join(_cbdir, 'ConebeamProjectionKernels.cu'), 'r').read()
import pycuda.autoinit
_mod = SourceModule(_f, no_extern_c=True)


def ConebeamProject(proj, vol, cf, ds=None, onesOnly=False):
    '''Conebeam Projection operator with fixed-step line integrals.
    Uses trilinear interpolation for the line integrals

    proj (PyCA Image3D): 2D projection image
    vol (PyCA Image3D): 3D PyCA Image3D volume to be interpolated
    cf (AppUtils config object): conebeam geometry parameters
    ds (float): integral step size. If 'None', step size is smartly chosen
    onesOnly (bool): If 'True', only project through a volume of ones'''

    if proj.size().z != 1:
        raise Exception('The projection image must be 2D')

    if proj.memType() != ca.MEM_DEVICE:
        raise Exception('Projection must be on the GPU')
    if vol.memType() != ca.MEM_DEVICE:
        raise Exception('Volume must be on the GPU')

    # Make sure config values are valid
    Config.RunValidationHooks(cf, ConebeamProjectionConfigSpecs)

    # get CUDA function
    CBPJ = _mod.get_function("ConebeamProjectionKernel")

    Imgrid = (int(np.ceil(float(proj.size().x)/Imblock[0])),
              int(np.ceil(float(proj.size().y)/Imblock[1])),
              1)

    if ds is None:
        ds = min(vol.spacing().x, vol.spacing().y, vol.spacing().z)

    # curved detector
    curved = False
    try:
        curved = cf.curved
    except AttributeError:
        pass

    CBPJ(np.uint64(proj.rawptr()),
         np.uint64(vol.rawptr()),
         np.uint32(vol.size().x),
         np.uint32(vol.size().y),
         np.uint32(vol.size().z),
         np.uint32(proj.size().x),
         np.uint32(proj.size().y),
         np.float32(vol.spacing().x),
         np.float32(vol.spacing().y),
         np.float32(vol.spacing().z),
         np.float32(proj.spacing().x),
         np.float32(proj.spacing().y),
         np.float32(vol.origin().x),  # volume origin in ITK Coords (World)
         np.float32(vol.origin().y),
         np.float32(vol.origin().z),
         np.float32(proj.origin().x),  # Proj origin in ITK Coords (Real)
         np.float32(proj.origin().y),
         np.float32(cf.cam_rot[(0, 0)]),  # camera to world rotation
         np.float32(cf.cam_rot[(0, 1)]),
         np.float32(cf.cam_rot[(0, 2)]),
         np.float32(cf.cam_rot[(1, 0)]),
         np.float32(cf.cam_rot[(1, 1)]),
         np.float32(cf.cam_rot[(1, 2)]),
         np.float32(cf.cam_rot[(2, 0)]),
         np.float32(cf.cam_rot[(2, 1)]),
         np.float32(cf.cam_rot[(2, 2)]),
         np.float32(cf.cam_trans[0]),  # camera to world translation
         np.float32(cf.cam_trans[1]),
         np.float32(cf.cam_trans[2]),
         np.float32(cf.piercing_point[0]),  # piercing point (Real)
         np.float32(cf.piercing_point[1]),
         np.float32(cf.sid),    # f
         np.float32(ds),        # ds - line integral step size
         np.uint32(onesOnly),
         np.uint32(curved),     # if detector is curved
         block=Imblock,
         grid=Imgrid)
    if sync:
        cuda.Context.synchronize() # for profiling


def ConebeamProjectFast(proj, vol, cf, onesOnly=False):
    '''Conebeam Projection operator with voxel edge interpolation.
    Uses bilinear interpolation for the line integrals, which is faster
    than trilinear interpolation from ConebeamProject

    proj (PyCA Image3D): 2D projection image
    vol (PyCA Image3D): 3D PyCA Image3D volume to be interpolated
    cf (AppUtils config object): conebeam geometry parameters
    onesOnly (bool): If 'True', only project through a volume of ones'''


    if proj.size().z != 1:
        raise Exception('The projection image must be 2D')
    if proj.memType() != ca.MEM_DEVICE:
        raise Exception('Projection must be on the GPU')
    if vol.memType() != ca.MEM_DEVICE:
        raise Exception('Volume must be on the GPU')

    # Make sure config values are valid
    Config.RunValidationHooks(cf, ConebeamProjectionConfigSpecs)

    # get CUDA function
    CBPJF = _mod.get_function("ConebeamProjectionFastKernel")

    Imgrid = (int(np.ceil(float(proj.size().x)/Imblock[0])),
              int(np.ceil(float(proj.size().y)/Imblock[1])),
              1)

    CBPJF(np.uint64(proj.rawptr()),
          np.uint64(vol.rawptr()),
          np.uint32(vol.size().x),
          np.uint32(vol.size().y),
          np.uint32(vol.size().z),
          np.uint32(proj.size().x),
          np.uint32(proj.size().y),
          np.float32(vol.spacing().x),
          np.float32(vol.spacing().y),
          np.float32(vol.spacing().z),
          np.float32(proj.spacing().x),
          np.float32(proj.spacing().y),
          np.float32(vol.origin().x),  # volume origin in ITK Coords (World)
          np.float32(vol.origin().y),
          np.float32(vol.origin().z),
          np.float32(proj.origin().x),  # Proj origin in ITK Coords (Real)
          np.float32(proj.origin().y),
          np.float32(cf.cam_rot[(0, 0)]),  # camera to world rotation
          np.float32(cf.cam_rot[(0, 1)]),
          np.float32(cf.cam_rot[(0, 2)]),
          np.float32(cf.cam_rot[(1, 0)]),
          np.float32(cf.cam_rot[(1, 1)]),
          np.float32(cf.cam_rot[(1, 2)]),
          np.float32(cf.cam_rot[(2, 0)]),
          np.float32(cf.cam_rot[(2, 1)]),
          np.float32(cf.cam_rot[(2, 2)]),
          np.float32(cf.cam_trans[0]),  # camera to world translation
          np.float32(cf.cam_trans[1]),
          np.float32(cf.cam_trans[2]),
          np.float32(cf.piercing_point[0]),  # piercing point (Real)
          np.float32(cf.piercing_point[1]),
          np.float32(cf.sid),   # f
          np.uint32(onesOnly),
          block=Imblock,
          grid=Imgrid)
    if sync:
        # cuda.Stream().synchronize() # doesn't work (cProfile)
        # cuda.Event().synchronize() # no
        # pycuda.autoinit.context.synchronize() #YES
        cuda.Context.synchronize()  # YES


def ConebeamBackproject(vol, proj, cf, mask=None, addResult=True,
                        onesOnly=False):
    '''Conebeam backprojection operator: voxel based.
    Calculates the backprojection at each voxel by using a bilinear
    interpolation of the projection grid. Significanlty faster than splatting.

    vol (PyCA Image3D): 3D PyCA Image3D volume to be interpolated
    proj (PyCA Image3D): 2D projection image
    cf (AppUtils config object): conebeam geometry parameters
    mask (optional 2D PyCA Image3D): binary projection image showing the
       backprojection region
    addResults (bool): If 'True', add the backprojection to vol
    onesOnly (bool): If 'True', only backproject a projection of ones'''


    # Make sure config values are valid
    Config.RunValidationHooks(cf, ConebeamProjectionConfigSpecs)

    if proj.size().z != 1:
        raise Exception('The projection image must be 2D')
    if proj.memType() != ca.MEM_DEVICE:
        raise Exception('Projection must be on the GPU')
    if vol.memType() != ca.MEM_DEVICE:
        raise Exception('Volume must be on the GPU')

    CBBPJ = _mod.get_function("ConebeamBackprojectionKernel")

    Imblock = (vol.size().x, 1, 1)
    Imgrid = (vol.size().y, vol.size().z, 1)

    if mask is None:
        maskPtr = 0
    else:
        maskPtr = mask.rawptr()
        assert mask.grid() == proj.grid()

    # curved detector
    curved = False
    try:
        curved = cf.curved
    except AttributeError:
        pass

    CBBPJ(np.uint64(vol.rawptr()),
          np.uint64(proj.rawptr()),
          np.uint64(maskPtr),
          np.uint32(vol.size().x),
          np.uint32(vol.size().y),
          np.uint32(vol.size().z),
          np.uint32(proj.size().x),
          np.uint32(proj.size().y),
          np.float32(vol.spacing().x),
          np.float32(vol.spacing().y),
          np.float32(vol.spacing().z),
          np.float32(proj.spacing().x),
          np.float32(proj.spacing().y),
          np.float32(vol.origin().x),  # volume origin in ITK Coords (World)
          np.float32(vol.origin().y),
          np.float32(vol.origin().z),
          np.float32(proj.origin().x),  # Proj origin in ITK Coords (Real)
          np.float32(proj.origin().y),
          np.float32(cf.cam_rot[(0, 0)]),  # camera to world rotation
          np.float32(cf.cam_rot[(0, 1)]),
          np.float32(cf.cam_rot[(0, 2)]),
          np.float32(cf.cam_rot[(1, 0)]),
          np.float32(cf.cam_rot[(1, 1)]),
          np.float32(cf.cam_rot[(1, 2)]),
          np.float32(cf.cam_rot[(2, 0)]),
          np.float32(cf.cam_rot[(2, 1)]),
          np.float32(cf.cam_rot[(2, 2)]),
          np.float32(cf.cam_trans[0]),  # camera to world translation
          np.float32(cf.cam_trans[1]),
          np.float32(cf.cam_trans[2]),
          np.float32(cf.piercing_point[0]),  # piercing point (Real)
          np.float32(cf.piercing_point[1]),
          np.float32(cf.sid),   # f
          np.uint32(addResult),
          np.uint32(onesOnly),
          np.uint32(curved),
          block=Imblock,
          grid=Imgrid)
    if sync:
        cuda.Context.synchronize()  # for profiling


def ConebeamBackprojectSplat(vol, proj, cf, ds=None, mask=None,
                             addResult=True, onesOnly=False):
    '''Conebeam backprojection operator: splatting based.
    This is the numerical adjoint of ConebeamProject, but since it
    uses splatting, it is slower than the voxel-based backprojection

    vol (PyCA Image3D): 3D volume
    proj (PyCA Image3D): 2D projection image
    cf (AppUtils config object): conebeam geometry parameters
    ds (float): line integral step size. If 'None', step size is smartly chosen
    mask (optional 2D PyCA Image3D): binary projection image showing the
       backprojection region
    addResults (bool): If 'True', add the backprojection to vol
    onesOnly (bool): If 'True', only backproject a projection of ones'''

    # Make sure config values are valid
    Config.RunValidationHooks(cf, ConebeamProjectionConfigSpecs)

    if proj.size().z != 1:
        raise Exception('The projection image must be 2D')
    if proj.memType() != ca.MEM_DEVICE:
        raise Exception('Projection must be on the GPU')
    if vol.memType() != ca.MEM_DEVICE:
        raise Exception('Volume must be on the GPU')

    CBBPJ = _mod.get_function("ConebeamBackprojectionKernelSplat")

    Imblock = (vol.size().x, 1, 1)
    Imgrid = (vol.size().y, vol.size().z, 1)

    if mask is None:
        maskPtr = 0
    else:
        maskPtr = mask.rawptr()
        assert mask.grid() == proj.grid()
        raise NotImplementedError

    if ds is None:
        ds = min(vol.spacing().x, vol.spacing().y, vol.spacing().z)

    CBBPJ(np.uint64(vol.rawptr()),
          np.uint64(proj.rawptr()),
          np.uint64(maskPtr),
          np.uint32(vol.size().x),
          np.uint32(vol.size().y),
          np.uint32(vol.size().z),
          np.uint32(proj.size().x),
          np.uint32(proj.size().y),
          np.float32(vol.spacing().x),
          np.float32(vol.spacing().y),
          np.float32(vol.spacing().z),
          np.float32(proj.spacing().x),
          np.float32(proj.spacing().y),
          np.float32(vol.origin().x),  # volume origin in ITK Coords (World)
          np.float32(vol.origin().y),
          np.float32(vol.origin().z),
          np.float32(proj.origin().x),  # Proj origin in ITK Coords (Real)
          np.float32(proj.origin().y),
          np.float32(cf.cam_rot[(0, 0)]),  # camera to world rotation
          np.float32(cf.cam_rot[(0, 1)]),
          np.float32(cf.cam_rot[(0, 2)]),
          np.float32(cf.cam_rot[(1, 0)]),
          np.float32(cf.cam_rot[(1, 1)]),
          np.float32(cf.cam_rot[(1, 2)]),
          np.float32(cf.cam_rot[(2, 0)]),
          np.float32(cf.cam_rot[(2, 1)]),
          np.float32(cf.cam_rot[(2, 2)]),
          np.float32(cf.cam_trans[0]),  # camera to world translation
          np.float32(cf.cam_trans[1]),
          np.float32(cf.cam_trans[2]),
          np.float32(cf.piercing_point[0]),  # piercing point (Real)
          np.float32(cf.piercing_point[1]),
          np.float32(cf.sid),   # f
          np.float32(ds),
          np.uint32(addResult),
          np.uint32(onesOnly),
          block=Imblock,
          grid=Imgrid)
    if sync:
        cuda.Context.synchronize()  # for profiling


def ConebeamPoseGradient(proj, vol, cf, ds=None, gradmask=None, mask=None):
    '''returns with the gradient with respect to Translation, Rotation, and
    intrinsic parameters for the volume estimate and a single projection

    The returned gradient is the gradient of the SSE between the forward
    projection of vol and proj.

    [(dTx, dTy, dTz), (dw0, dw1, dw2), (du0, dv0, df), sum_squared_error] =
                             ConebeamPoseGradient(proj, vol, cf)

    The rotation gradient consists of the entries of the skew-symmetric
    matrix and must be exponentiated to apply the gradient

    proj (PyCA Image3D): 2D projection image
    vol (PyCA Image3D): 3D PyCA Image3D volume to be interpolated
    cf (AppUtils config object): conebeam geometry parameters
    ds (float): integral step size. If 'None', step size is smartly chosen
    gradmask (PyCA Image3D): 3D binary mask corresponding to vol
    mask (PyCA Image3D): 2D binary mask corresponding to proj
'''


    if proj.size().z != 1:
        raise Exception('The projection image must be 2D')
    if proj.memType() != ca.MEM_DEVICE:
        raise Exception('Projection must be on the GPU')
    if vol.memType() != ca.MEM_DEVICE:
        raise Exception('Volume must be on the GPU')

    # Make sure config values are valid
    Config.RunValidationHooks(cf, ConebeamProjectionConfigSpecs)

    # get CUDA function
    CBgrad = _mod.get_function("PoseGradientKernel")

    Imgrid = (int(np.ceil(float(proj.size().x)/Imblock[0])),
              int(np.ceil(float(proj.size().y)/Imblock[1])),
              1)

    grads = np.zeros((Imgrid[0]*Imgrid[1], 10), np.float32)
    ds = min(vol.spacing().x, vol.spacing().y, vol.spacing().z)  # step size

    if gradmask is not None:
        gradmaskptr = gradmask.rawptr()
    else:
        gradmaskptr = 0
    if mask is None:
        maskPtr = 0
    else:
        assert mask.grid() == proj.grid()
        maskPtr = mask.rawptr()

    CBgrad(cuda.InOut(grads),
           np.uint64(proj.rawptr()),
           np.uint64(maskPtr),
           np.uint64(vol.rawptr()),
           np.uint64(gradmaskptr),
           np.uint32(vol.size().x),
           np.uint32(vol.size().y),
           np.uint32(vol.size().z),
           np.uint32(proj.size().x),
           np.uint32(proj.size().y),
           np.float32(vol.spacing().x),
           np.float32(vol.spacing().y),
           np.float32(vol.spacing().z),
           np.float32(proj.spacing().x),
           np.float32(proj.spacing().y),
           np.float32(vol.origin().x),  # volume origin in ITK Coords (World)
           np.float32(vol.origin().y),
           np.float32(vol.origin().z),
           np.float32(proj.origin().x),  # Proj origin in ITK Coords (Real)
           np.float32(proj.origin().y),
           np.float32(cf.cam_rot[(0, 0)]),  # camera to world rotation
           np.float32(cf.cam_rot[(0, 1)]),
           np.float32(cf.cam_rot[(0, 2)]),
           np.float32(cf.cam_rot[(1, 0)]),
           np.float32(cf.cam_rot[(1, 1)]),
           np.float32(cf.cam_rot[(1, 2)]),
           np.float32(cf.cam_rot[(2, 0)]),
           np.float32(cf.cam_rot[(2, 1)]),
           np.float32(cf.cam_rot[(2, 2)]),
           np.float32(cf.cam_trans[0]),  # camera to world translation
           np.float32(cf.cam_trans[1]),
           np.float32(cf.cam_trans[2]),
           np.float32(cf.piercing_point[0]),  # piercing point (Real)
           np.float32(cf.piercing_point[1]),
           np.float32(cf.sid),  # f
           np.float32(ds),      # ds - line integral step size
           block=Imblock,
           grid=Imgrid)

    grads_sum = np.sum(grads, axis=0)
    # normalize by number of elements
    grads_sum /= proj.nVox()

    # adjust translation for rotation
    grads_sum[0:3] = np.dot(cf.cam_rot.T, grads_sum[0:3])  # NewCS
    # also rotate PP/SID
    grads_sum[6:9] = np.dot(cf.cam_rot.T, grads_sum[6:9])

    grads_sum[6:8] *= -1
    grads_sum[9] *= .5          # we define energy as 1/2(diff)^2

    if sync:
        cuda.Context.synchronize()  # for profiling

    return grads_sum


def ConebeamPoseGradientNCC(proj, vol, cf, gradmask=None,
                            mask=None, calcParamIndex=None):
    '''returns with the gradient with respect to Translation, Rotation, and
    intrinsic parameters for the volume estimate and a single projection

    The returned gradient is the gradient of the local NCC between the forward
    projection of vol and proj.

    [(dTx, dTy, dTz), (dw0, dw1, dw2), (du0, dv0, df), NCC] =
                             ConebeamPoseGradient(proj, vol, cf)

    The rotation gradient consists of the entries of the skew-symmetric
    matrix and must be exponentiated to apply the gradient

    proj (PyCA Image3D): 2D projection image
    vol (PyCA Image3D): 3D PyCA Image3D volume to be interpolated
    cf (AppUtils config object): conebeam geometry parameters
    ds (float): integral step size. If 'None', step size is smartly chosen
    gradmask (PyCA Image3D): 3D binary mask corresponding to vol
    mask (PyCA Image3D): 2D binary mask corresponding to proj
    '''

    if proj.size().z != 1:
        raise Exception('The projection image must be 2D')
    if proj.memType() != ca.MEM_DEVICE:
        raise Exception('Projection must be on the GPU')
    if vol.memType() != ca.MEM_DEVICE:
        raise Exception('Volume must be on the GPU')

    if calcParamIndex is None:
        calcParamIndex = [True]*10

    Tx = ca.Image3D(proj.grid(), ca.MEM_DEVICE)
    Ty = ca.Image3D(proj.grid(), ca.MEM_DEVICE)
    Tz = ca.Image3D(proj.grid(), ca.MEM_DEVICE)
    R0 = ca.Image3D(proj.grid(), ca.MEM_DEVICE)
    R1 = ca.Image3D(proj.grid(), ca.MEM_DEVICE)
    R2 = ca.Image3D(proj.grid(), ca.MEM_DEVICE)
    I0 = ca.Image3D(proj.grid(), ca.MEM_DEVICE)
    I1 = ca.Image3D(proj.grid(), ca.MEM_DEVICE)
    I2 = ca.Image3D(proj.grid(), ca.MEM_DEVICE)
    simproj = ca.Image3D(proj.grid(), ca.MEM_DEVICE)

    # Make sure config values are valid
    Config.RunValidationHooks(cf, ConebeamProjectionConfigSpecs)

    # get CUDA function
    CBgrad = _mod.get_function("PoseGradientKernelImages")

    Imgrid = (int(np.ceil(float(proj.size().x)/Imblock[0])),
              int(np.ceil(float(proj.size().y)/Imblock[1])),
              1)

    ds = min(vol.spacing().x, vol.spacing().y, vol.spacing().z)  # step size

    if gradmask is not None:
        gradmaskptr = gradmask.rawptr()
    else:
        gradmaskptr = 0
    if mask is None:
        maskPtr = 0
    else:
        assert mask.grid() == proj.grid()
        maskPtr = mask.rawptr()

    CBgrad(np.uint64(Tx.rawptr()),
           np.uint64(Ty.rawptr()),
           np.uint64(Tz.rawptr()),
           np.uint64(R0.rawptr()),
           np.uint64(R1.rawptr()),
           np.uint64(R2.rawptr()),
           np.uint64(I0.rawptr()),
           np.uint64(I1.rawptr()),
           np.uint64(I2.rawptr()),
           np.uint64(simproj.rawptr()),
           np.uint64(proj.rawptr()),
           np.uint64(maskPtr),
           np.uint64(vol.rawptr()),
           np.uint64(gradmaskptr),
           np.uint32(vol.size().x),
           np.uint32(vol.size().y),
           np.uint32(vol.size().z),
           np.uint32(proj.size().x),
           np.uint32(proj.size().y),
           np.float32(vol.spacing().x),
           np.float32(vol.spacing().y),
           np.float32(vol.spacing().z),
           np.float32(proj.spacing().x),
           np.float32(proj.spacing().y),
           np.float32(vol.origin().x),  # volume origin in ITK Coords (World)
           np.float32(vol.origin().y),
           np.float32(vol.origin().z),
           np.float32(proj.origin().x),  # Proj origin in ITK Coords (Real)
           np.float32(proj.origin().y),
           np.float32(cf.cam_rot[(0, 0)]),  # camera to world rotation
           np.float32(cf.cam_rot[(0, 1)]),
           np.float32(cf.cam_rot[(0, 2)]),
           np.float32(cf.cam_rot[(1, 0)]),
           np.float32(cf.cam_rot[(1, 1)]),
           np.float32(cf.cam_rot[(1, 2)]),
           np.float32(cf.cam_rot[(2, 0)]),
           np.float32(cf.cam_rot[(2, 1)]),
           np.float32(cf.cam_rot[(2, 2)]),
           np.float32(cf.cam_trans[0]),  # camera to world translation
           np.float32(cf.cam_trans[1]),
           np.float32(cf.cam_trans[2]),
           np.float32(cf.piercing_point[0]),  # piercing point (Real)
           np.float32(cf.piercing_point[1]),
           np.float32(cf.sid),  # f
           np.float32(ds),      # ds - line integral step size
           block=Imblock,
           grid=Imgrid)

    # save mem/computation time if user doesn't want these gradients
    if not calcParamIndex[0]:
        Tx = None
    if not calcParamIndex[1]:
        Ty = None
    if not calcParamIndex[2]:
        Tz = None
    if not calcParamIndex[3]:
        R0 = None
    if not calcParamIndex[4]:
        R1 = None
    if not calcParamIndex[5]:
        R2 = None
    if not calcParamIndex[6]:
        I0 = None
    if not calcParamIndex[7]:
        I1 = None
    if not calcParamIndex[8]:
        I2 = None

    # Do NCC
    Pbar, fbar, kPf, kPdP, kdPf, kP2, kf2, num, denom, tmp = \
        [ca.Image3D(simproj.grid(), ca.MEM_DEVICE) for _ in xrange(10)]

    grads_sum = np.zeros(10)

    size = int(10.0/(512/vol.size().x))*2+1  # fix this for now...
    # size = [size, size, 1]
    # size = [21, 21, 1]

    localNCC = proj.size() < 64
    # localNCC = False

    # import PyCACalebExtras.Common as cc
    import util as cc

    if localNCC:
        cc.RectFilter(tmp, simproj, size)
        ca.Sub(Pbar, simproj, tmp)
        cc.RectFilter(tmp, proj, size)
        ca.Sub(fbar, proj, tmp)

        ca.Mul(tmp, Pbar, fbar)
        cc.RectFilter(kPf, tmp, size)
        ca.Mul(tmp, Pbar, Pbar)
        cc.RectFilter(kP2, tmp, size)
        ca.Mul(tmp, fbar, fbar)
        cc.RectFilter(kf2, tmp, size)
    else:
        ca.SubC(Pbar, simproj, ca.Sum(simproj)/simproj.nVox())
        ca.SubC(fbar, proj, ca.Sum(proj)/proj.nVox())
        ca.Mul(kPf, Pbar, fbar)
        kP2sum = ca.Sum2(Pbar)
        kf2sum = ca.Sum2(fbar)
        kPfsum = ca.Sum(kPf)

    for i, dP in enumerate([Tx, Ty, Tz, R0, R1, R2, I0, I1, I2]):
        if dP is None:
            continue

        if localNCC:
            ca.Mul(tmp, Pbar, dP)
            cc.RectFilter(kPdP, tmp, size)
            ca.Mul(tmp, fbar, dP)
            cc.RectFilter(kdPf, tmp, size)

            ca.Mul(num, kdPf, kP2)
            ca.Mul(tmp, kPf, kPdP)
            num -= tmp
            ca.PowC(denom, kP2, 1.5)
            ca.Sqrt(tmp, kf2)
            denom *= tmp

            cc.SetRegionLT(denom, denom, 0.001, 0.001)  # safe divide
            num /= denom

            grads_sum[i] = ca.Sum(num)/proj.nVox()
        else:
            ca.Mul(kPdP, Pbar, dP)
            ca.Mul(kdPf, fbar, dP)

            grads_sum[i] = ((kP2sum*ca.Sum(kdPf) - kPfsum*ca.Sum(kPdP)) /
                            (kP2sum**1.5*np.sqrt(kf2sum)))

    # calc NCC
    if localNCC:
        ca.Sqrt_I(kP2)
        ca.Sqrt_I(kf2)
        kP2 *= kf2
        cc.SetRegionLT(kP2, kP2, 0.001, 0.001)
        ca.Div(num, kPf, kP2)
        grads_sum[9] = ca.Sum(num)/proj.nVox()
    else:
        grads_sum[9] = kPfsum/np.sqrt(kP2sum*kf2sum)

    grads_sum *= -1.0           # make this a minimization problem

    if sync:
        cuda.Context.synchronize()  # for profiling

    return grads_sum


def ConebeamNCC(projdata, vol, cf, gradmask=None, mask=None, returnImage=False):
    '''returns the (negative) NCC, or the positive NCC image if
    'returnImage' is true

    projdata (PyCA Image3D): 2D projection image
    vol (PyCA Image3D): 3D PyCA Image3D volume to be interpolated
    cf (AppUtils config object): conebeam geometry parameters
    onesOnly (bool): If 'True', only project through a volume of ones'''

    volmasked = vol.copy()
    if gradmask is not None:
        volmasked *= gradmask

    simproj = ca.Image3D(projdata.grid(), ca.MEM_DEVICE)

    ConebeamProject(simproj, volmasked, cf)

    # Do NCC
    Pbar, fbar, kPf, kP2, kf2, num, denom, tmp = \
        [ca.Image3D(simproj.grid(), ca.MEM_DEVICE) for _ in xrange(8)]

    size = int(10.0/(512/vol.size().x))*2+1  # fix this for now...
    size = [size, size, 1]
    # size = [21, 21, 1]

    localNCC = projdata.size() < 64
    # localNCC = False

    # import PyCACalebExtras.Common as cc
    import util as cc

    if localNCC:
        cc.RectFilter(tmp, simproj, size)
        ca.Sub(Pbar, simproj, tmp)
        cc.RectFilter(tmp, projdata, size)
        ca.Sub(fbar, projdata, tmp)

        ca.Mul(tmp, Pbar, fbar)
        cc.RectFilter(kPf, tmp, size)
        ca.Mul(tmp, Pbar, Pbar)
        cc.RectFilter(kP2, tmp, size)
        ca.Mul(tmp, fbar, fbar)
        cc.RectFilter(kf2, tmp, size)

        # calc NCC
        ca.Sqrt_I(kP2)
        ca.Sqrt_I(kf2)
        kP2 *= kf2
        cc.SetRegionLT(kP2, kP2, 0.001, 0.001)
        ca.Div(num, kPf, kP2)

        ncc = ca.Sum(num)/num.nVox()
    else:
        ca.SubC(Pbar, simproj, ca.Sum(simproj)/simproj.nVox())
        ca.SubC(fbar, projdata, ca.Sum(projdata)/projdata.nVox())
        ca.Mul(kPf, Pbar, fbar)
        # ca.Mul(kP2, Pbar, Pbar)
        # ca.Mul(kf2, fbar, fbar)
        ncc = ca.Sum(kPf)/np.sqrt(ca.Sum2(Pbar)*ca.Sum2(fbar))

    if sync:
        cuda.Context.synchronize()  # for profiling

    if returnImage:
        return num
    return -ncc


def ConebeamSumSquaredError(proj, vol, cf):
    '''returns the SSE between the projection and volume

    proj (PyCA Image3D): 2D projection image
    vol (PyCA Image3D): 3D PyCA Image3D volume to be interpolated
    cf (AppUtils config object): conebeam geometry parameters
    '''

    if proj.size().z != 1:
        raise Exception('The projection image must be 2D')
    if proj.memType() != ca.MEM_DEVICE:
        raise Exception('Projection must be on the GPU')
    if vol.memType() != ca.MEM_DEVICE:
        raise Exception('Volume must be on the GPU')

    # Make sure config values are valid
    Config.RunValidationHooks(cf, ConebeamProjectionConfigSpecs)

    # get CUDA function
    CB_SSE = _mod.get_function("SumSquaredErrorKernel")

    Imgrid = (int(np.ceil(float(proj.size().x)/Imblock[0])),
              int(np.ceil(float(proj.size().y)/Imblock[1])),
              1)

    sse = np.zeros((Imgrid[0]*Imgrid[1]), np.float32)
    ds = min(vol.spacing().x, vol.spacing().y, vol.spacing().z)

    CB_SSE(cuda.InOut(sse),
           np.uint64(proj.rawptr()),
           np.uint64(vol.rawptr()),
           np.uint32(vol.size().x),
           np.uint32(vol.size().y),
           np.uint32(vol.size().z),
           np.uint32(proj.size().x),
           np.uint32(proj.size().y),
           np.float32(vol.spacing().x),
           np.float32(vol.spacing().y),
           np.float32(vol.spacing().z),
           np.float32(proj.spacing().x),
           np.float32(proj.spacing().y),
           np.float32(vol.origin().x),  # volume origin in ITK Coords (World)
           np.float32(vol.origin().y),
           np.float32(vol.origin().z),
           np.float32(proj.origin().x),  # Proj origin in ITK Coords (Real)
           np.float32(proj.origin().y),
           np.float32(cf.cam_rot[(0, 0)]),  # camera to world rotation
           np.float32(cf.cam_rot[(0, 1)]),
           np.float32(cf.cam_rot[(0, 2)]),
           np.float32(cf.cam_rot[(1, 0)]),
           np.float32(cf.cam_rot[(1, 1)]),
           np.float32(cf.cam_rot[(1, 2)]),
           np.float32(cf.cam_rot[(2, 0)]),
           np.float32(cf.cam_rot[(2, 1)]),
           np.float32(cf.cam_rot[(2, 2)]),
           np.float32(cf.cam_trans[0]),  # camera to world translation
           np.float32(cf.cam_trans[1]),
           np.float32(cf.cam_trans[2]),
           np.float32(cf.piercing_point[0]),  # piercing point (Real)
           np.float32(cf.piercing_point[1]),
           np.float32(cf.sid),  # f
           np.float32(ds),      # ds - line integral step size
           block=Imblock,
           grid=Imgrid)

    sse_sum = np.sum(sse)
    # normalize by number of elements?
    sse_sum /= proj.nVox()
    sse_sum *= .5               # energy is defined as 1/2(diff)^2

    if sync:
        cuda.Context.synchronize()  # for profiling

    return sse_sum


def ConebeamSumSquaredErrorFast(proj, vol, cf):
    '''returns with the Sum of Squared errors between the projection
    and the volume to be projected (typically estimated).

    Uses the same projection method as ConebeamProjectFast

    proj (PyCA Image3D): 2D projection image
    vol (PyCA Image3D): 3D PyCA Image3D volume to be interpolated
    cf (AppUtils config object): conebeam geometry parameters
    '''


    if proj.size().z != 1:
        raise Exception('The projection image must be 2D')
    if proj.memType() != ca.MEM_DEVICE:
        raise Exception('Projection must be on the GPU')
    if vol.memType() != ca.MEM_DEVICE:
        raise Exception('Volume must be on the GPU')

    # Make sure config values are valid
    Config.RunValidationHooks(cf, ConebeamProjectionConfigSpecs)

    # get CUDA function
    CB_SSEF = _mod.get_function("SumSquaredErrorFastKernel")

    Imgrid = (int(np.ceil(float(proj.size().x)/Imblock[0])),
              int(np.ceil(float(proj.size().y)/Imblock[1])),
              1)

    sse = np.zeros((Imgrid[0]*Imgrid[1]), np.float32)

    CB_SSEF(cuda.InOut(sse),
            np.uint64(proj.rawptr()),
            np.uint64(vol.rawptr()),
            np.uint32(vol.size().x),
            np.uint32(vol.size().y),
            np.uint32(vol.size().z),
            np.uint32(proj.size().x),
            np.uint32(proj.size().y),
            np.float32(vol.spacing().x),
            np.float32(vol.spacing().y),
            np.float32(vol.spacing().z),
            np.float32(proj.spacing().x),
            np.float32(proj.spacing().y),
            np.float32(vol.origin().x),  # volume origin in ITK Coords (World)
            np.float32(vol.origin().y),
            np.float32(vol.origin().z),
            np.float32(proj.origin().x),  # Proj origin in ITK Coords (Real)
            np.float32(proj.origin().y),
            np.float32(cf.cam_rot[(0, 0)]),  # camera to world rotation
            np.float32(cf.cam_rot[(0, 1)]),
            np.float32(cf.cam_rot[(0, 2)]),
            np.float32(cf.cam_rot[(1, 0)]),
            np.float32(cf.cam_rot[(1, 1)]),
            np.float32(cf.cam_rot[(1, 2)]),
            np.float32(cf.cam_rot[(2, 0)]),
            np.float32(cf.cam_rot[(2, 1)]),
            np.float32(cf.cam_rot[(2, 2)]),
            np.float32(cf.cam_trans[0]),  # camera to world translation
            np.float32(cf.cam_trans[1]),
            np.float32(cf.cam_trans[2]),
            np.float32(cf.piercing_point[0]),  # piercing point (Real)
            np.float32(cf.piercing_point[1]),
            np.float32(cf.sid),  # f
            block=Imblock,
            grid=Imgrid)

    sse_sum = np.sum(sse)
    # normalize by number of elements?
    sse_sum /= proj.nVox()
    sse_sum *= .5               # energy is defined as 1/2(diff)^2

    if sync:
        cuda.Context.synchronize()  # for profiling

    return sse_sum


def ComputeTVDerivative(d_vol, vol, eps=1e-5, cbmod=None):
    '''Computes the TV derivative of a volume.
    For checkerboard updates, let cbmod = 0 or 1

    Uses the stencil from Panin, Zeng, and Gullberg 1999
    '''

    VolTV = _mod.get_function("ComputeTVDerivative")

    Imblock = (vol.size().x, 1, 1)
    Imgrid = (vol.size().y, vol.size().z, 1)

    if cbmod is None:
        cbmod = 3               # invalid mod

    VolTV(np.uint64(d_vol.rawptr()),
          np.uint64(vol.rawptr()),
          np.uint32(vol.size().x),
          np.uint32(vol.size().y),
          np.uint32(vol.size().z),
          np.float32(vol.spacing().x),
          np.float32(vol.spacing().y),
          np.float32(vol.spacing().z),
          np.float32(eps),
          np.uint32(cbmod),
          block=Imblock,
          grid=Imgrid)

    if sync:
        cuda.Context.synchronize()  # for profiling
