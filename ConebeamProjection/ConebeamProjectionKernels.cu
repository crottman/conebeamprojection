// "Copyright 2016 Caleb Rottman"
// jdh 2009
// crr 2013

#include <stdio.h>
#include <math.h>
#include <cuda_runtime.h>
#include <algorithm>

// use gaussian likelihood for calibration (this is a little more stable
// possibly)
#define GAUSSIAN_CALIB



__device__ float
RealArrayGet3D(const float* d_vol,
               uint vNx, uint vNy, uint vNz,
               uint x, uint y, uint z) {
  return d_vol[(z*vNy+y)*vNx+x];
}


__device__ float
RealArrayGet2D(const float* d_arr, uint pNx, uint pNy, uint x, uint y) {
  return d_arr[y*pNx+x];
}


// nearest neighbor value w/ bounadry check
__device__ float
RealArrayGet3DNN(const float* d_vol,
                 uint vNx, uint vNy, uint vNz,
                 float x, float y, float  z) {
  int xi, yi, zi;
  xi = static_cast<int>(round(x));
  yi = static_cast<int>(round(y));
  zi = static_cast<int>(round(z));
  if (xi <0 || xi >= vNx || yi <0 || yi >= vNy || zi <0 || zi >= vNx) {
    return 0.0;
  } else {
    return d_vol[(zi*vNy+yi)*vNx+xi];
  }
}


__device__ float
TrilerpWeight(uint vNx, uint vNy, uint vNz,
              float x, float y, float z,
              uint i, uint j, uint k) {
  int floorX = static_cast<int>(x);
  int floorY = static_cast<int>(y);
  int floorZ = static_cast<int>(z);
  if (x < 0 && x != static_cast<int>(x)) --floorX;
  if (y < 0 && y != static_cast<int>(y)) --floorY;
  if (z < 0 && z != static_cast<int>(z)) --floorZ;

  // this is not truly ceiling, but floor + 1, which is usually ceiling
  int ceilX = floorX + 1;
  int ceilY = floorY + 1;
  int ceilZ = floorZ + 1;

  float w;

  if (i == floorX)
    w = static_cast<float>(ceilX) - x;
  else if (i == ceilX)
    w = x - static_cast<float>(floorX);
  else   // if we're not looking at a neighboring voxel then forget it
    return 0.0f;

  if (j == floorY)
    w *= static_cast<float>(ceilY) - y;
  else if (j == ceilY)
    w *= y - static_cast<float>(floorY);
  else
    return 0.0f;

  if (k == floorZ)
    w *= static_cast<float>(ceilZ) - z;
  else if (k == ceilZ)
    w *= z - static_cast<float>(floorZ);
  else
    return 0.0f;

  return w;
}


__device__ float
RealTrilerp(const float* d_vol,
            uint vNx, uint vNy, uint vNz,
            float x, float y, float z, float background = 0.0f) {
  // TRILERP
  int floorX = static_cast<int>(x);
  int floorY = static_cast<int>(y);
  int floorZ = static_cast<int>(z);
  if (x < 0 && x != static_cast<int>(x)) --floorX;
  if (y < 0 && y != static_cast<int>(y)) --floorY;
  if (z < 0 && z != static_cast<int>(z)) --floorZ;

  // this is not truly ceiling, but floor + 1, which is usually ceiling
  int ceilX = floorX + 1;
  int ceilY = floorY + 1;
  int ceilZ = floorZ + 1;

  //
  // ^
  // |  v3   v2       -z->        v4   v5
  // y           --next slice-->
  // |  v0   v1                   v7   v6
  //
  //      -x->
  //
  float v0, v1, v2, v3, v4, v5, v6, v7;

  if (floorX >= 0 && ceilX < static_cast<int>(vNx) &&
      floorY >= 0 && ceilY < static_cast<int>(vNy) &&
      floorZ >= 0 && ceilZ < static_cast<int>(vNz)) {
    // this is the fast path
    v0 = RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, floorY, floorZ);
    v1 = RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, floorY, floorZ);
    v2 = RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, ceilY, floorZ);
    v3 = RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, ceilY, floorZ);
    v4 = RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, ceilY, ceilZ);
    v5 = RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, ceilY, ceilZ);
    v6 = RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, floorY, ceilZ);
    v7 = RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, floorY, ceilZ);
  } else {
    bool floorXIn = floorX >= 0 && floorX < static_cast<int>(vNx);
    bool floorYIn = floorY >= 0 && floorY < static_cast<int>(vNy);
    bool floorZIn = floorZ >= 0 && floorZ < static_cast<int>(vNz);

    bool ceilXIn = ceilX >= 0 && ceilX < static_cast<int>(vNx);
    bool ceilYIn = ceilY >= 0 && ceilY < static_cast<int>(vNy);
    bool ceilZIn = ceilZ >= 0 && ceilZ < static_cast<int>(vNz);

    v0 = (floorXIn && floorYIn && floorZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, floorY, floorZ):background;
    v1 = (ceilXIn && floorYIn && floorZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, floorY, floorZ) :background;
    v2 = (ceilXIn && ceilYIn && floorZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, ceilY, floorZ)  :background;
    v3 = (floorXIn && ceilYIn && floorZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, ceilY, floorZ) :background;
    v4 = (floorXIn && ceilYIn && ceilZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, ceilY, ceilZ)  :background;
    v5 = (ceilXIn && ceilYIn && ceilZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, ceilY, ceilZ)   :background;
    v6 = (ceilXIn && floorYIn && ceilZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, floorY, ceilZ)  :background;
    v7 = (floorXIn && floorYIn && ceilZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, floorY, ceilZ) :background;
  }

  const float t = x - floorX;
  const float u = y - floorY;
  const float v = z - floorZ;
  const float oneMinusT = 1.0 - t;
  const float oneMinusU = 1.0 - u;
  const float oneMinusV = 1.0 - v;

  //
  // this is the basic trilerp function...
  //
  //     val =
  //       v0 * (1 - t) * (1 - u) * (1 - v) +
  //       v1 * t       * (1 - u) * (1 - v) +
  //       v2 * t       * u       * (1 - v) +
  //       v3 * (1 - t) * u       * (1 - v) +
  //       v4 * (1 - t) * u       * v       +
  //       v5 * t       * u       * v       +
  //       v6 * t       * (1 - u) * v       +
  //       v7 * (1 - t) * (1 - u) * v;
  //
  // the following saves some computation...
  //

  return
    oneMinusT * (oneMinusU * (v0 * oneMinusV + v7 * v)  +
                 u         * (v3 * oneMinusV + v4 * v)) +
    t         * (oneMinusU * (v1 * oneMinusV + v6 * v)  +
                 u         * (v2 * oneMinusV + v5 * v));
}


__device__ float
realTrilerpWithGradient(const float* d_vol,
                        uint vNx, uint vNy, uint vNz,
                        float x, float y, float z,
                        float& delIx, float& delIy, float& delIz,
                        float background = 0.0f) {
  // TRILERP
  int floorX = static_cast<int>(x);
  int floorY = static_cast<int>(y);
  int floorZ = static_cast<int>(z);
  if (x < 0 && x != static_cast<int>(x)) --floorX;
  if (y < 0 && y != static_cast<int>(y)) --floorY;
  if (z < 0 && z != static_cast<int>(z)) --floorZ;

  // this is not truly ceiling, but floor + 1, which is usually ceiling
  int ceilX = floorX + 1;
  int ceilY = floorY + 1;
  int ceilZ = floorZ + 1;

  //
  // ^
  // |  v3   v2       -z->        v4   v5
  // y           --next slice-->
  // |  v0   v1                   v7   v6
  //
  //      -x->
  //
  float v0, v1, v2, v3, v4, v5, v6, v7;

  if (floorX >= 0 && ceilX < static_cast<int>(vNx) &&
      floorY >= 0 && ceilY < static_cast<int>(vNy) &&
      floorZ >= 0 && ceilZ < static_cast<int>(vNz)) {
    // this is the fast path
    v0 = RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, floorY, floorZ);
    v1 = RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, floorY, floorZ);
    v2 = RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, ceilY, floorZ);
    v3 = RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, ceilY, floorZ);
    v4 = RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, ceilY, ceilZ);
    v5 = RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, ceilY, ceilZ);
    v6 = RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, floorY, ceilZ);
    v7 = RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, floorY, ceilZ);
  } else {
    bool floorXIn = floorX >= 0 && floorX < static_cast<int>(vNx);
    bool floorYIn = floorY >= 0 && floorY < static_cast<int>(vNy);
    bool floorZIn = floorZ >= 0 && floorZ < static_cast<int>(vNz);

    bool ceilXIn = ceilX >= 0 && ceilX < static_cast<int>(vNx);
    bool ceilYIn = ceilY >= 0 && ceilY < static_cast<int>(vNy);
    bool ceilZIn = ceilZ >= 0 && ceilZ < static_cast<int>(vNz);

    v0 = (floorXIn && floorYIn && floorZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, floorY, floorZ):background;
    v1 = (ceilXIn && floorYIn && floorZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, floorY, floorZ) :background;
    v2 = (ceilXIn && ceilYIn && floorZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, ceilY, floorZ)  :background;
    v3 = (floorXIn && ceilYIn && floorZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, ceilY, floorZ) :background;
    v4 = (floorXIn && ceilYIn && ceilZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, ceilY, ceilZ)  :background;
    v5 = (ceilXIn && ceilYIn && ceilZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, ceilY, ceilZ)   :background;
    v6 = (ceilXIn && floorYIn && ceilZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, floorY, ceilZ)  :background;
    v7 = (floorXIn && floorYIn && ceilZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, floorY, ceilZ) :background;
  }

  const float t = x - floorX;
  const float u = y - floorY;
  const float v = z - floorZ;
  const float oneMinusT = 1.0 - t;
  const float oneMinusU = 1.0 - u;
  const float oneMinusV = 1.0 - v;

  //
  // this is the basic trilerp function...
  //
  //     val =
  //       v0 * (1 - t) * (1 - u) * (1 - v) +
  //       v1 * t       * (1 - u) * (1 - v) +
  //       v2 * t       * u       * (1 - v) +
  //       v3 * (1 - t) * u       * (1 - v) +
  //       v4 * (1 - t) * u       * v       +
  //       v5 * t       * u       * v       +
  //       v6 * t       * (1 - u) * v       +
  //       v7 * (1 - t) * (1 - u) * v;
  //
  // the following saves some computation...
  //

  // compute gradient
  delIx = - oneMinusU*oneMinusV * v0
    + oneMinusU*oneMinusV * v1
    +         u*oneMinusV * v2
    -         u*oneMinusV * v3
    -         u*        v * v4
    +         u*        v * v5
    + oneMinusU*        v * v6
    - oneMinusU*        v * v7;
  delIy = - oneMinusT*oneMinusV * v0
    -         t*oneMinusV * v1
    +         t*oneMinusV * v2
    + oneMinusT*oneMinusV * v3
    + oneMinusT*        v * v4
    +         t*        v * v5
    -         t*        v * v6
    - oneMinusT*        v * v7;
  delIz = - oneMinusT*oneMinusU * v0
    -         t*oneMinusU * v1
    -         t*        u * v2
    - oneMinusT*        u * v3
    + oneMinusT*        u * v4
    +         t*        u * v5
    +         t*oneMinusU * v6
    + oneMinusT*oneMinusU * v7;

  return
    oneMinusT * (oneMinusU * (v0 * oneMinusV + v7 * v)  +
                 u         * (v3 * oneMinusV + v4 * v)) +
    t         * (oneMinusU * (v1 * oneMinusV + v6 * v)  +
                 u         * (v2 * oneMinusV + v5 * v));
}


__device__ float
RealBilerp(const float* d_proj,
           uint pNx, uint pNy,
           float x, float y,
           float background = 0.0f) {
  // a faster version of the floor function
  int floorX = static_cast<int>(x);
  int floorY = static_cast<int>(y);

  if (x < 0 && x != static_cast<int>(x)) --floorX;
  if (y < 0 && y != static_cast<int>(y)) --floorY;

  // this is not truly ceiling, but floor + 1, which is usually ceiling
  int ceilX = floorX + 1;
  int ceilY = floorY + 1;

  //
  // ^
  // |  v3   v2       -z->        v4   v5
  // y           --next slice-->
  // |  v0   v1                   v7   v6
  //
  //      -x->
  //
  float v0, v1, v2, v3;

  if (floorX >= 0 && ceilX < static_cast<int>(pNx) &&
      floorY >= 0 && ceilY < static_cast<int>(pNy)) {
    // this is the fast path
    v0 = RealArrayGet2D(d_proj, pNx, pNy, floorX, floorY);
    v1 = RealArrayGet2D(d_proj, pNx, pNy, ceilX, floorY);
    v2 = RealArrayGet2D(d_proj, pNx, pNy, ceilX, ceilY);
    v3 = RealArrayGet2D(d_proj, pNx, pNy, floorX, ceilY);
  } else {
    bool floorXIn = floorX >= 0 && floorX < static_cast<int>(pNx);
    bool floorYIn = floorY >= 0 && floorY < static_cast<int>(pNy);
    bool ceilXIn = ceilX >= 0 && ceilX < static_cast<int>(pNx);
    bool ceilYIn = ceilY >= 0 && ceilY < static_cast<int>(pNy);

    v0 = (floorXIn && floorYIn)
      ? RealArrayGet2D(d_proj, pNx, pNy, floorX, floorY) : background;
    v1 = (ceilXIn && floorYIn)
      ? RealArrayGet2D(d_proj, pNx, pNy, ceilX, floorY)  : background;
    v2 = (ceilXIn && ceilYIn)
      ? RealArrayGet2D(d_proj, pNx, pNy, ceilX, ceilY)   : background;
    v3 = (floorXIn && ceilYIn)
      ? RealArrayGet2D(d_proj, pNx, pNy, floorX, ceilY)  : background;
  }

  const float t = x - floorX;
  const float u = y - floorY;

  const float oneMinusT = 1.0 - t;
  const float oneMinusU = 1.0 - u;

  //
  // this is the basic bilerp function...
  //
  //     val =
  //       v0 * (1 - t) * (1 - u)
  //       v1 * t       * (1 - u)
  //       v2 * t       * u
  //       v3 * (1 - t) * u
  //
  // the following saves some computation...
  //

  return oneMinusT*(oneMinusU*v0 + u*v3) + t*(oneMinusU*v1 + u*v2);
}


__device__ float
RealBilerp3Dx(const float* d_vol,
              uint vNx, uint vNy, uint vNz,
              uint xi, float y, float z, float background = 0.0f) {
  int floorY = static_cast<int>(y);
  int floorZ = static_cast<int>(z);
  if (y < 0 && y != static_cast<int>(y)) --floorY;
  if (z < 0 && z != static_cast<int>(z)) --floorZ;

  // this is not truly ceiling, but floor + 1, which is usually ceiling
  int ceilY = floorY + 1;
  int ceilZ = floorZ + 1;

  float v0, v1, v2, v3;

  if (floorY >= 0 && ceilY < static_cast<int>(vNy) &&
      floorZ >= 0 && ceilZ < static_cast<int>(vNz)) {
    // this is the fast path
    v0 = RealArrayGet3D(d_vol, vNx, vNy, vNz, xi, floorY, floorZ);
    v1 = RealArrayGet3D(d_vol, vNx, vNy, vNz, xi, ceilY, floorZ);
    v2 = RealArrayGet3D(d_vol, vNx, vNy, vNz, xi, ceilY, ceilZ);
    v3 = RealArrayGet3D(d_vol, vNx, vNy, vNz, xi, floorY, ceilZ);
  } else {
    bool floorYIn = floorY >= 0 && floorY < static_cast<int>(vNy);
    bool floorZIn = floorZ >= 0 && floorZ < static_cast<int>(vNz);

    bool ceilYIn = ceilY >= 0 && ceilY < static_cast<int>(vNy);
    bool ceilZIn = ceilZ >= 0 && ceilZ < static_cast<int>(vNz);

    v0 = (floorYIn && floorZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, xi, floorY, floorZ) : background;
    v1 = (ceilYIn && floorZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, xi, ceilY, floorZ)  : background;
    v2 = (ceilYIn && ceilZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, xi, ceilY, ceilZ)   : background;
    v3 = (floorYIn && ceilZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, xi, floorY, ceilZ)  : background;
  }

  const float t = y - floorY;
  const float u = z - floorZ;
  const float oneMinusT = 1.0 - t;
  const float oneMinusU = 1.0 - u;

  return oneMinusT*(oneMinusU*v0 + u*v3) + t*(oneMinusU*v1 + u*v2);
}

__device__ float
RealBilerp3Dy(const float* d_vol,
              uint vNx, uint vNy, uint vNz,
              float x, uint yi, float z, float background = 0.0f) {
  int floorX = static_cast<int>(x);
  int floorZ = static_cast<int>(z);
  if (x < 0 && x != static_cast<int>(x)) --floorX;
  if (z < 0 && z != static_cast<int>(z)) --floorZ;

  // this is not truly ceiling, but floor + 1, which is usually ceiling
  int ceilX = floorX + 1;
  int ceilZ = floorZ + 1;

  float v0, v1, v2, v3;

  if (floorX >= 0 && ceilX < static_cast<int>(vNx) &&
      floorZ >= 0 && ceilZ < static_cast<int>(vNz)) {
    // this is the fast path
    v0 = RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, yi, floorZ);
    v1 = RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, yi, floorZ);
    v2 = RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, yi, ceilZ);
    v3 = RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, yi, ceilZ);
  } else {
    bool floorXIn = floorX >= 0 && floorX < static_cast<int>(vNx);
    bool floorZIn = floorZ >= 0 && floorZ < static_cast<int>(vNz);

    bool ceilXIn = ceilX >= 0 && ceilX < static_cast<int>(vNx);
    bool ceilZIn = ceilZ >= 0 && ceilZ < static_cast<int>(vNz);

    v0 = (floorXIn && floorZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, yi, floorZ) : background;
    v1 = (ceilXIn && floorZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, yi, floorZ)  : background;
    v2 = (ceilXIn && ceilZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, yi, ceilZ)   : background;
    v3 = (floorXIn && ceilZIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, yi, ceilZ)  : background;
  }

  const float t = x - floorX;
  const float u = z - floorZ;
  const float oneMinusT = 1.0 - t;
  const float oneMinusU = 1.0 - u;

  return oneMinusT*(oneMinusU*v0 + u*v3) + t*(oneMinusU*v1 + u*v2);
}


__device__ float
RealBilerp3Dz(const float* d_vol,
              uint vNx, uint vNy, uint vNz,
              float x, float y, uint zi, float background = 0.0f) {
  int floorX = static_cast<int>(x);
  int floorY = static_cast<int>(y);
  if (x < 0 && x != static_cast<int>(x)) --floorX;
  if (y < 0 && y != static_cast<int>(y)) --floorY;

  // this is not truly ceiling, but floor + 1, which is usually ceiling
  int ceilX = floorX + 1;
  int ceilY = floorY + 1;

  float v0, v1, v2, v3;

  if (floorX >= 0 && ceilX < static_cast<int>(vNx) &&
      floorY >= 0 && ceilY < static_cast<int>(vNy)) {
    // this is the fast path
    v0 = RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, floorY, zi);
    v1 = RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, floorY, zi);
    v2 = RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, ceilY, zi);
    v3 = RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, ceilY, zi);
  } else {
    bool floorXIn = floorX >= 0 && floorX < static_cast<int>(vNx);
    bool floorYIn = floorY >= 0 && floorY < static_cast<int>(vNy);

    bool ceilXIn = ceilX >= 0 && ceilX < static_cast<int>(vNx);
    bool ceilYIn = ceilY >= 0 && ceilY < static_cast<int>(vNy);

    v0 = (floorXIn && floorYIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, floorY, zi) : background;
    v1 = (ceilXIn && floorYIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, floorY, zi)  : background;
    v2 = (ceilXIn && ceilYIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, ceilX, ceilY, zi)   : background;
    v3 = (floorXIn && ceilYIn)
      ? RealArrayGet3D(d_vol, vNx, vNy, vNz, floorX, ceilY, zi)  : background;
  }

  const float t = x - floorX;
  const float u = y - floorY;
  const float oneMinusT = 1.0 - t;
  const float oneMinusU = 1.0 - u;

  return oneMinusT*(oneMinusU*v0 + u*v3) + t*(oneMinusU*v1 + u*v2);
}


extern "C" __global__ void
ConebeamProjectionKernel(float* d_proj,
                         const float* d_vol,
                         // vol/proj size
                         uint vNx, uint vNy, uint vNz, uint pNx, uint pNy,
                         // vol/proj spacing
                         float vdx, float vdy, float vdz, float pdx, float pdy,
                         // vol/proj origin in real coordinates
                         float vOx, float vOy, float vOz, float pOx, float pOy,
                         // camera to world rotation
                         float R00, float R01, float R02,
                         float R10, float R11, float R12,
                         float R20, float R21, float R22,
                         // camera to world translation
                         float Tx, float Ty, float Tz,
                         float u0,  float v0,  // piercing point (real coords)
                         float f,              // SID
                         float ds,             // line integral step size
                         uint onesOnly,
                         uint curved) {         // curved detector
  // get index in projection image from blockIdx and threadIdx
  uint px = blockIdx.x*blockDim.x + threadIdx.x;
  uint py = blockIdx.y*blockDim.y + threadIdx.y;

  if (px >= pNx || py >= pNy) return;  // outside projection

  // set up source and pixel in camera coords
  float projxC = px*pdx + pOx - u0;
  float projyC = py*pdy + pOy - v0;
  float projzC = f;

  // adjust locations on the detector
  if (curved) {
    projzC = f * cos(projxC/f);
    projxC = f * sin(projxC/f);
  }

  // convert to world coords with R(x+T)
  float sourcexW = R00*Tx + R01*Ty + R02*Tz;
  float sourceyW = R10*Tx + R11*Ty + R12*Tz;
  float sourcezW = R20*Tx + R21*Ty + R22*Tz;
  float projxW = R00*(projxC+Tx) + R01*(projyC+Ty) + R02*(projzC+Tz);
  float projyW = R10*(projxC+Tx) + R11*(projyC+Ty) + R12*(projzC+Tz);
  float projzW = R20*(projxC+Tx) + R21*(projyC+Ty) + R22*(projzC+Tz);

  // find direction of line integral
  float dx = projxW - sourcexW;
  float dy = projyW - sourceyW;
  float dz = projzW - sourcezW;
  float dist = sqrt(dx*dx+dy*dy+dz*dz);

  // normalize integral direction
  float normfactor = 1.0f/dist;
  dx *= normfactor;
  dy *= normfactor;
  dz *= normfactor;

  // Determine lmin and lmax as intersections of cylinder with line
  /*float dxcosg = xrayDist*d/sqrt(yr*yr+d*d);*/
  /*float r = 0.5f*(static_cast<float>(pNx-1))*pdx;*/
  /*float root = sqrt(dxcosg*dxcosg - (xrayDist*xrayDist - r*r));*/
  /*float lmin = dxcosg - root;*/
  /*float lmax = dxcosg + root;*/
  /*float lfactor = sqrt(projz*projz+d*d)/d;*/
  /*lmin *= lfactor;*/
  /*lmax *= lfactor;*/
  float lmin = 0;
  float lmax = dist;
  // uint numsteps = (uint)(dist/ds);
  uint numsteps = (uint)((lmax-lmin)/ds)+1;

  // start at source
  float x = sourcexW;
  float y = sourceyW;
  float z = sourcezW;

  // Now convert to index coords
  dx *= ds/vdx;
  dy *= ds/vdy;
  dz *= ds/vdz;

  // convert x, y, z to index
  x = (x-vOx)/vdx;
  y = (y-vOy)/vdy;
  z = (z-vOz)/vdz;

  float projVal = 0.0f;

  for (uint k = 0; k < numsteps; ++k) {
    if (x <= -1 || y <= -1 || z <= -1 ||
        x >= vNx || y >= vNy || z >= vNz) {
      // increment current position along line
      x += dx;
      y += dy;
      z += dz;
      continue;             // 0 out here, not need to Trilerp
    }
    // trilerp to get value
    projVal += onesOnly? 1.0f : RealTrilerp(d_vol, vNx, vNy, vNz, x, y, z, 0.0f);

    // increment current position along line
    x += dx;
    y += dy;
    z += dz;
  }

  // complete the integral by multiplying by incremental length
  d_proj[py*pNx + px] = projVal*ds;
}


extern "C" __global__ void
ConebeamProjectionFastKernel(float* d_proj,
                             const float* d_vol,
                             // vol/proj size
                             uint vNx, uint vNy, uint vNz, uint pNx, uint pNy,
                             // vol/proj spacing
                             float vdx, float vdy, float vdz,
                             float pdx, float pdy,
                             // vol/proj origin in real coordinates
                             float vOx, float vOy, float vOz,
                             float pOx, float pOy,
                             // camera to world rotation
                             float R00, float R01, float R02,
                             float R10, float R11, float R12,
                             float R20, float R21, float R22,
                             // camera to world translation
                             float Tx, float Ty, float Tz,
                             float u0, float v0,  // piercing point-real coords
                             float f,             // SID
                             uint onesOnly) {
  // get index in projection image from blockIdx and threadIdx
  uint px = blockIdx.x*blockDim.x + threadIdx.x;
  uint py = blockIdx.y*blockDim.y + threadIdx.y;

  if (px >= pNx || py >= pNy) return;  // outside projection

  // set up source and pixel in camera coords
  float projxC = px*pdx + pOx - u0;
  float projyC = py*pdy + pOy - v0;
  float projzC = f;

  // convert to world coords with R(x+T)
  float sourcexW = R00*Tx + R01*Ty + R02*Tz;
  float sourceyW = R10*Tx + R11*Ty + R12*Tz;
  float sourcezW = R20*Tx + R21*Ty + R22*Tz;
  float projxW = R00*(projxC+Tx) + R01*(projyC+Ty) + R02*(projzC+Tz);
  float projyW = R10*(projxC+Tx) + R11*(projyC+Ty) + R12*(projzC+Tz);
  float projzW = R20*(projxC+Tx) + R21*(projyC+Ty) + R22*(projzC+Tz);

  // convert to index coords
  float sourcexI = (sourcexW - vOx)/vdx;
  float sourceyI = (sourceyW - vOy)/vdy;
  float sourcezI = (sourcezW - vOz)/vdz;
  float projxI = (projxW - vOx)/vdx;
  float projyI = (projyW - vOy)/vdy;
  float projzI = (projzW - vOz)/vdz;

  // find direction of line integral
  float dx = projxI - sourcexI;
  float dy = projyI - sourceyI;
  float dz = projzI - sourcezI;
  float dist = sqrt(dx*dx+dy*dy+dz*dz);

  // normalize integral direction
  float normfactor = 1.0f/dist;
  dx *= normfactor;
  dy *= normfactor;
  dz *= normfactor;

  float projVal = 0.0f;

  float x, y, z, realstepx2, realstepy2, realstepz2;

  if (abs(dx) > abs(dy) && abs(dx) > abs(dz)) {
    for (uint xi = 0; xi < vNx; ++xi) {
      // get y and z values in index coords
      y = sourceyI + (xi - sourcexI)*dy/dx;
      z = sourcezI + (xi - sourcexI)*dz/dx;
      if (y <= -1 || z <= -1 || y >= vNy || z >= vNz)
        continue;             // 0 out here, not need to Trilerp
      // bilerp to get value
      projVal += onesOnly ? 1.0f : RealBilerp3Dx(d_vol, vNx, vNy, vNz, xi, y, z, 0.0f);
    }
    // modify for actual length of step (3D pythagorean)
    realstepx2 = vdx*vdx;
    realstepy2 = vdy*vdy*dy*dy/dx/dx;
    realstepz2 = vdz*vdz*dz*dz/dx/dx;
  } else if (abs(dy) > abs(dz)) {
    for (uint yi = 0; yi < vNy; ++yi) {
      x = sourcexI + (yi - sourceyI)*dx/dy;
      z = sourcezI + (yi - sourceyI)*dz/dy;
      if (x <= -1 || z <= -1 || x >= vNx || z >= vNz)
        continue;             // 0 out here, not need to Trilerp
      projVal += onesOnly ? 1.0f : RealBilerp3Dy(d_vol, vNx, vNy, vNz, x, yi, z, 0.0f);
    }
    realstepx2 = vdx*vdx*dx*dx/dy/dy;
    realstepy2 = vdy*vdy;
    realstepz2 = vdz*vdz*dz*dz/dy/dy;
  } else {
    for (uint zi = 0; zi < vNz; ++zi) {
      x = sourcexI + (zi - sourcezI)*dx/dz;
      y = sourceyI + (zi - sourcezI)*dy/dz;
      if (x <= -1 || y <= -1 || x >= vNx || y >= vNy)
        continue;             // 0 out here, not need to Trilerp
      projVal += onesOnly ? 1.0f : RealBilerp3Dz(d_vol, vNx, vNy, vNz, x, y, zi, 0.0f);
    }
    realstepx2 = vdx*vdx*dx*dx/dz/dz;
    realstepy2 = vdy*vdy*dy*dy/dz/dz;
    realstepz2 = vdz*vdz;
  }
  projVal *= sqrt(realstepx2 + realstepy2 + realstepz2);
  // complete the integral by multiplying by incremental length
  d_proj[py*pNx + px] = projVal;
}


extern "C" __global__ void
ConebeamBackprojectionKernel(float* d_vol,
                             const float* d_proj,
                             const float* d_mask,
                             // vol/proj size
                             uint vNx, uint vNy, uint vNz, uint pNx, uint pNy,
                             // vol/proj spacing
                             float vdx, float vdy, float vdz,
                             float pdx, float pdy,
                             // vol/proj origin in real coordinates
                             float vOx, float vOy, float vOz,
                             float pOx, float pOy,
                             // camera to world rotation
                             float R00, float R01, float R02,
                             float R10, float R11, float R12,
                             float R20, float R21, float R22,
                             // camera to world translation
                             float Tx, float Ty, float Tz,
                             float u0, float v0,  // piercing point--real coords
                             float f,  // SID
                             uint addResult,  // Add to Volume?
                             uint onesOnly,
                             uint curved) {  // curved detector
  // This is a voxel-based backprojection
  uint vx = threadIdx.x;
  uint vy = blockIdx.x;
  uint vz = blockIdx.y;
  if (vx >= vNx || vy >= vNy || vz >= vNz) return;  // outside volume

  // convert to world coords from index coords
  float vxW = static_cast<float>(vx)*vdx + vOx;
  float vyW = static_cast<float>(vy)*vdy + vOy;
  float vzW = static_cast<float>(vz)*vdz + vOz;

  // convert into camera coords using R'*(x-T)
  float vxC = R00*vxW + R10*vyW + R20*vzW - Tx;
  float vyC = R01*vxW + R11*vyW + R21*vzW - Ty;
  float vzC = R02*vxW + R12*vyW + R22*vzW - Tz;

  // Get scale (distance from source)
  // Note: I don't actually use this scale, because it cancels out in OSEM
  // float r = sqrt(vxC*vxC + vyC*vyC + vzC*vzC);
  // if (vzC*vzC > 1.0){               // use actual scale
  //   scale = f*f/(vzC*vzC*vzC)*r;
  // }
  float scale = 1.0;

  // find projection position in camera coords
  float perpScale;  // ratio until ray hits detector
  if (curved) {
    perpScale = f/sqrt(vxC*vxC + vzC*vzC);
  } else {
    perpScale = f/vzC;
  }
  if (vzC == 0.0) {               // in case source is inside image, avoid nan
    perpScale = 0.0;
  }

  vxC *= perpScale;
  vyC *= perpScale;

  // un-curve detector (in x only)
  if (curved) {
    vxC = f * asin(vxC/f);
  }

  // convert to index coordinates for projection
  vxC = (-pOx + u0 + vxC)/pdx;
  vyC = (-pOy + v0 + vyC)/pdy;

  float bgval = 0.0f;  // hardcode for now

  float projval;
  if (vxC < 0 || vyC < 0 || vxC > pNx-1 || vyC > pNy-1 ||vzC < 0 || vzC > f) {
    projval = bgval;
  } else {
    if (onesOnly) {
      projval = d_mask ? RealBilerp(d_mask, pNx, pNy, vxC, vyC, bgval) : 1.0f;
    } else {
      projval = RealBilerp(d_proj, pNx, pNy, vxC, vyC, bgval);
    }
  }
  // float maskval;
  // if (vxC < 0 || vyC < 0 || vxC > pNx-1 || vyC > pNy-1 ||vzC < 0 || vzC > f) {
  //   projval = bgval;
  // } else {
  //   if (d_mask) {
  //     maskval = (RealBilerp(d_mask, pNx, pNy, vxC, vyC, bgval) < .99f) ? 0.0f : 1.0f;
  //   } else {
  //     maskval = 1.0f;
  //   }

  //   // a lot of logic here, i think it works out though...
  //   if (onesOnly) {
  //     if ((bgval == 0.0f) && (maskval == 0.0f)) {
  //       projval == 0.0f;
  //     } else {
  //       projval == 1.0f;
  //     }
  //   } else {
  //     if ((bgval == 1.0f) && (maskval == 0.0f)) {
  //       projval == 1.0f;
  //     } else {
  //       projval == maskval*RealBilerp(d_proj, pNx, pNy, vxC, vyC, bgval);
  //     }
  //   }
  // }

  projval *= scale;             // account for distance

  if (addResult)
    d_vol[vz*vNx*vNy+vy*vNx+vx] += projval;
  else                          // just set to this value
    d_vol[vz*vNx*vNy+vy*vNx+vx] = projval;
}


extern "C" __global__ void
ConebeamBackprojectionKernelSplat(float* d_vol,
                                  const float* d_proj,
                                  const float* d_mask,
                                  // volume/detector sizes
                                  uint vNx, uint vNy, uint vNz,
                                  uint pNx, uint pNy,
                                  // volume/detector voxel spacings
                                  float vdx, float vdy, float vdz,
                                  float pdx, float pdy,
                                  // volume/detector origins
                                  float vOx, float vOy, float vOz,
                                  float pOx, float pOy,
                                  // camera to world rotation matrix
                                  float R00, float R01, float R02,
                                  float R10, float R11, float R12,
                                  float R20, float R21, float R22,
                                  // camera to world translation
                                  float Tx, float Ty, float Tz,
                                  float u0, float v0,  // piercing point (real)
                                  float f,             // SID
                                  float ds,  // line integral step size
                                  uint addResult,  // Add to volume?
                                  uint onesOnly) {
  // This is a projection-based backprojection (exact adjoint)
  // however, we do not ray march and splat because we'd need atomic addition
  // which will be very slow in this case due to blocking.  Instead, we visit
  // each voxel, be smart about finding which rays will contribute, and splat

  uint vx = threadIdx.x;
  uint vy = blockIdx.x;
  uint vz = blockIdx.y;

  if (vx >= vNx || vy >= vNy || vz >= vNz) return;  // outside volume

  // convert to world coords
  float vxW = static_cast<float>(vx)*vdx + vOx;
  float vyW = static_cast<float>(vy)*vdy + vOy;
  float vzW = static_cast<float>(vz)*vdz + vOz;

  // convert into camera coords using R'*(x-T)
  float vxC = R00*vxW + R10*vyW + R20*vzW - Tx;
  float vyC = R01*vxW + R11*vyW + R21*vzW - Ty;
  float vzC = R02*vxW + R12*vyW + R22*vzW - Tz;
  float vdist = sqrt(vxC*vxC + vyC*vyC + vzC*vzC);
  // always need source in world coords
  float sourcexW = R00*Tx + R01*Ty + R02*Tz;
  float sourceyW = R10*Tx + R11*Ty + R12*Tz;
  float sourcezW = R20*Tx + R21*Ty + R22*Tz;

  // But source should be in index coordinates (crr)
  // float sourcexI = (sourcexW - vOx)/vdx; // for "projection"
  // float sourceyI = (sourceyW - vOy)/vdy;
  // float sourcezI = (sourcezW - vOz)/vdz;

  // Now we only need to consider a ball of radius as large as the biggest
  // voxel spacing, projected onto the projection plane
  float rincV = 0.5*sqrt(vdx*vdx+vdy*vdy+vdz*vdz);  // world coords
  // rincV *= 3.0;                                     // testing
  // pseudo-project sphere onto projection grid coords
  float rincPx = (rincV*f/vzC/pdx);  // this is in pixel coords
  float rincPy = (rincV*f/vzC/pdy);

  // visit every pixel in the projection plane within rincP distance from the
  // projected voxel
  // point through center of voxel on detector
  float projxCi = ((f/vzC)*vxC - pOx + u0)/pdx;  // detector index coords
  float projyCi = ((f/vzC)*vyC - pOy + v0)/pdy;

  float voxelValue = 0.0;
  for (int py = max(0.0, floor(projyCi-rincPy));
       py < min(static_cast<float>(pNy), ceil(projyCi+rincPy));
       ++py)
    for (int px = max(0.0, floor(projxCi-rincPx));
         px < min(static_cast<float>(pNx), ceil(projxCi+rincPx));
         ++px) {       // For each ray that might intersect this voxel
      // Get world coordinates of projection point
      float projxC = px*pdx + pOx - u0;
      float projyC = py*pdy + pOy - v0;
      float projzC = f;
      float projxW = R00*(projxC+Tx) + R01*(projyC+Ty) + R02*(projzC+Tz);
      float projyW = R10*(projxC+Tx) + R11*(projyC+Ty) + R12*(projzC+Tz);
      float projzW = R20*(projxC+Tx) + R21*(projyC+Ty) + R22*(projzC+Tz);

      // find direction of line integral in world coords
      float dx = projxW - sourcexW;
      float dy = projyW - sourceyW;
      float dz = projzW - sourcezW;
      float dist = sqrt(dx*dx+dy*dy+dz*dz);
      // normalize integral direction (still world coords)
      float normfactor = 1.0f/dist;
      dx *= normfactor;
      dy *= normfactor;
      dz *= normfactor;
      // convert dx to index coords
      dx *= ds/vdx;
      dy *= ds/vdy;
      dz *= ds/vdz;

      // find range in which to ray-march
      float lminW = vdist-rincV;  // not "snapped"
      float lmaxW = vdist+rincV;
      float lminI = floor(lminW/ds);  // now snapped in integral index
      float lmaxI = ceil(lmaxW/ds);
      lminW = lminI*ds;
      lmaxW = lmaxI*ds;
      int numsteps = static_cast<uint>(lmaxI-lminI);

      float x = sourcexW;
      float y = sourceyW;
      float z = sourcezW;
      x = (x-vOx)/vdx + lminI*dx;        // convert x to index coords
      y = (y-vOy)/vdy + lminI*dy;
      z = (z-vOz)/vdz + lminI*dz;

      // For each voxel in ray segment
      for (uint i = 0; i <= numsteps; ++i) {
        if (x < vx-0.5 || y < vy-0.5 || z < vz-0.5 ||
            x > vx+0.5 || y > vy+0.5 || z > vz+0.5) {
          x += dx;       // only increment current position along line
          y += dy;
          z += dz;
          continue;             // point on line not in voxel boundary
        }

        // Get Trilerp coefficent
        float w = TrilerpWeight(vNx, vNy, vNz, x, y, z, vx, vy, vz);
        if (d_mask)
          w *= d_mask[py*pNx+px];

        // sum
        if (onesOnly)
          voxelValue += w;
        else
          voxelValue += w*d_proj[py*pNx+px];

        x += dx;              // increment current position along line
        y += dy;
        z += dz;
      }
    }

  // record result
  if (addResult)
    d_vol[vz*vNx*vNy + vy*vNx + vx] += ds*voxelValue;
  else
    d_vol[vz*vNx*vNy + vy*vNx + vx] = ds*voxelValue;
}


extern "C" __global__ void
SumSquaredErrorKernel(float* d_err,
                      const float* d_projdata,
                      const float* d_vol,
                      // vol/proj size
                      uint vNx, uint vNy, uint vNz, uint pNx, uint pNy,
                      // vol/proj spacing
                      float vdx, float vdy, float vdz, float pdx, float pdy,
                      // vol/proj origin in real coordinates
                      float vOx, float vOy, float vOz, float pOx, float pOy,
                      // camera to world rotation
                      float R00, float R01, float R02,
                      float R10, float R11, float R12,
                      float R20, float R21, float R22,
                      // camera to world translation
                      float Tx, float Ty, float Tz,
                      float u0,  float v0,  // piercing point (real coords)
                      float f,  // SID
                      float ds) {  // line integral step size
  // get index in projection image from blockIdx and threadIdx
  uint px = blockIdx.x*blockDim.x + threadIdx.x;
  uint py = blockIdx.y*blockDim.y + threadIdx.y;
  uint blockNum = blockIdx.y*gridDim.x + blockIdx.x;
  // number the threads within same block
  uint numThreads = blockDim.x*blockDim.y*blockDim.z;
  uint threadNum = threadIdx.y*blockDim.x + threadIdx.x;

  // note that trying to allocate more might cause probs
  __shared__ __volatile__ float s_err[1024];

  if (threadNum == 0)           // supervisor thread
    for (uint i = 0; i < numThreads; i++)
      s_err[i] = 0.0f;

  __syncthreads();

  if (px >= pNx || py >= pNy) return;  // outside projection

  // set up source and pixel in camera coords
  float projxC = px*pdx + pOx - u0;
  float projyC = py*pdy + pOy - v0;
  float projzC = f;

  // convert to world coords with R(x+T)
  float sourcexW = R00*Tx + R01*Ty + R02*Tz;
  float sourceyW = R10*Tx + R11*Ty + R12*Tz;
  float sourcezW = R20*Tx + R21*Ty + R22*Tz;
  float projxW = R00*(projxC+Tx) + R01*(projyC+Ty) + R02*(projzC+Tz);
  float projyW = R10*(projxC+Tx) + R11*(projyC+Ty) + R12*(projzC+Tz);
  float projzW = R20*(projxC+Tx) + R21*(projyC+Ty) + R22*(projzC+Tz);

  // find direction of line integral
  float dx = projxW - sourcexW;
  float dy = projyW - sourceyW;
  float dz = projzW - sourcezW;
  float dist = sqrt(dx*dx+dy*dy+dz*dz);

  // normalize integral direction
  float normfactor = 1.0f/dist;
  dx *= normfactor;
  dy *= normfactor;
  dz *= normfactor;

  // Determine lmin and lmax as intersections of cylinder with line
  /*float dxcosg = xrayDist*d/sqrt(yr*yr+d*d);*/
  /*float r = 0.5f*(static_cast<float>(pNx-1))*pdx;*/
  /*float root = sqrt(dxcosg*dxcosg - (xrayDist*xrayDist - r*r));*/
  /*float lmin = dxcosg - root;*/
  /*float lmax = dxcosg + root;*/
  /*float lfactor = sqrt(projz*projz+d*d)/d;*/
  /*lmin *= lfactor;*/
  /*lmax *= lfactor;*/
  float lmin = 0;
  float lmax = dist;
  uint numsteps = (uint)((lmax-lmin)/ds)+1;

  // start at distance lmin from the source
  float x = sourcexW + lmin*dx;
  float y = sourceyW + lmin*dy;
  float z = sourcezW + lmin*dz;

  // Now convert to index coords
  dx *= ds/vdx;
  dy *= ds/vdy;
  dz *= ds/vdz;

  // convert to index
  x = (x-vOx)/vdx;
  y = (y-vOy)/vdy;
  z = (z-vOz)/vdz;

  float projVal = 0.0f;

  for (uint k = 0; k < numsteps; ++k) {
    if (x <= -1 || y <= -1 || z <= -1 ||
        x >= vNx || y >= vNy || z >= vNz) {
      // increment current position along line
      x += dx;
      y += dy;
      z += dz;
      continue;             // 0 out here, not need to Trilerp
    }

    // trilerp to get value
    projVal += RealTrilerp(d_vol, vNx, vNy, vNz, x, y, z, 0.0f);

    // increment current position along line
    x += dx;
    y += dy;
    z += dz;
  }

  // record SSE for this pixel
#ifdef GAUSSIAN_CALIB
  float sse = d_projdata[py*pNx+px]-ds*projVal;
  s_err[threadNum] += sse*sse;
#else  // poisson
  s_err[threadNum] += d_projdata[py*pNx+px]*log(ds*projVal + 1e-5)-ds*projVal;
#endif

  __syncthreads();

  if (threadNum == 0) {
    // now accumulate this block and put into d_grads
    float acc_SSE = 0.0f;
    for (uint i = 0; i < numThreads; i++)
      acc_SSE += s_err[i];
    d_err[blockNum] = acc_SSE;
  }
}


// SumSquaredError, but bilerps smartly instead of trilerping
extern "C" __global__ void
SumSquaredErrorFastKernel(float* d_err,
                          const float* d_projdata,
                          const float* d_vol,
                          // vol/proj size
                          uint vNx, uint vNy, uint vNz, uint pNx, uint pNy,
                          // vol/proj spacing
                          float vdx, float vdy, float vdz, float pdx, float pdy,
                          // vol/proj origin in real coordinates
                          float vOx, float vOy, float vOz, float pOx, float pOy,
                          // camera to world rotation
                          float R00, float R01, float R02,
                          float R10, float R11, float R12,
                          float R20, float R21, float R22,
                          // camera to world translation
                          float Tx, float Ty, float Tz,
                          float u0, float v0,  // piercing point (real coords)
                          float f) {            // SID
  // get index in projection image from blockIdx and threadIdx
  uint px = blockIdx.x*blockDim.x + threadIdx.x;
  uint py = blockIdx.y*blockDim.y + threadIdx.y;
  uint blockNum = blockIdx.y*gridDim.x + blockIdx.x;
  // number the threads within same block
  uint numThreads = blockDim.x*blockDim.y*blockDim.z;
  uint threadNum = threadIdx.y*blockDim.x + threadIdx.x;

  // note that trying to allocate more might cause probs
  __shared__ __volatile__ float s_err[1024];

  if (threadNum == 0)           // supervisor thread
    for (uint i = 0; i < numThreads; i++)
      s_err[i] = 0.0f;

  __syncthreads();

  if (px >= pNx || py >= pNy) return;  // outside projection

  // set up source and pixel in camera coords
  float projxC = px*pdx + pOx - u0;
  float projyC = py*pdy + pOy - v0;
  float projzC = f;

  // convert to world coords with R(x+T)
  float sourcexW = R00*Tx + R01*Ty + R02*Tz;
  float sourceyW = R10*Tx + R11*Ty + R12*Tz;
  float sourcezW = R20*Tx + R21*Ty + R22*Tz;
  float projxW = R00*(projxC+Tx) + R01*(projyC+Ty) + R02*(projzC+Tz);
  float projyW = R10*(projxC+Tx) + R11*(projyC+Ty) + R12*(projzC+Tz);
  float projzW = R20*(projxC+Tx) + R21*(projyC+Ty) + R22*(projzC+Tz);

  // convert to index coords
  float sourcexI = (sourcexW - vOx)/vdx;
  float sourceyI = (sourceyW - vOy)/vdy;
  float sourcezI = (sourcezW - vOz)/vdz;
  float projxI = (projxW - vOx)/vdx;
  float projyI = (projyW - vOy)/vdy;
  float projzI = (projzW - vOz)/vdz;

  // find direction of line integral
  float dx = projxI - sourcexI;
  float dy = projyI - sourceyI;
  float dz = projzI - sourcezI;
  float dist = sqrt(dx*dx+dy*dy+dz*dz);

  // normalize integral direction
  float normfactor = 1.0f/dist;
  dx *= normfactor;
  dy *= normfactor;
  dz *= normfactor;

  float projVal = 0.0f;

  float x, y, z, realstepx2, realstepy2, realstepz2;

  if (abs(dx) > abs(dy) && abs(dx) > abs(dz)) {
    for (uint xi = 0; xi < vNx; ++xi) {
      // get y and z values in index coords
      y = sourceyI + (xi - sourcexI)*dy/dx;
      z = sourcezI + (xi - sourcexI)*dz/dx;
      if (y <= -1 || z <= -1 || y >= vNy || z >= vNz)
        continue;             // 0 out here, not need to Trilerp
      // bilerp to get value
      projVal += RealBilerp3Dx(d_vol, vNx, vNy, vNz, xi, y, z, 0.0f);
    }
    // modify for actual length of step (3D pythagorean)
    realstepx2 = vdx*vdx;
    realstepy2 = vdy*vdy*dy*dy/dx/dx;
    realstepz2 = vdz*vdz*dz*dz/dx/dx;
  } else if (abs(dy) > abs(dz)) {
    for (uint yi = 0; yi < vNy; ++yi) {
      x = sourcexI + (yi - sourceyI)*dx/dy;
      z = sourcezI + (yi - sourceyI)*dz/dy;
      if (x <= -1 || z <= -1 || x >= vNx || z >= vNz)
        continue;             // 0 out here, not need to Trilerp
      projVal += RealBilerp3Dy(d_vol, vNx, vNy, vNz, x, yi, z, 0.0f);
    }
    realstepx2 = vdx*vdx*dx*dx/dy/dy;
    realstepy2 = vdy*vdy;
    realstepz2 = vdz*vdz*dz*dz/dy/dy;
  } else {
    for (uint zi = 0; zi < vNz; ++zi) {
      x = sourcexI + (zi - sourcezI)*dx/dz;
      y = sourceyI + (zi - sourcezI)*dy/dz;
      if (x <= -1 || y <= -1 || x >= vNx || y >= vNy)
        continue;             // 0 out here, not need to Trilerp
      projVal += RealBilerp3Dz(d_vol, vNx, vNy, vNz, x, y, zi, 0.0f);
    }
    realstepx2 = vdx*vdx*dx*dx/dz/dz;
    realstepy2 = vdy*vdy*dy*dy/dz/dz;
    realstepz2 = vdz*vdz;
  }
  projVal *= sqrt(realstepx2 + realstepy2 + realstepz2);

  // record SSE for this pixel
  float sse = d_projdata[py*pNx+px]-projVal;
  s_err[threadNum] += sse*sse;

  __syncthreads();

  if (threadNum == 0) {
    // now accumulate this block and put into d_grads
    float acc_SSE = 0.0f;
    for (uint i = 0; i < numThreads; i++)
      acc_SSE += s_err[i];
    d_err[blockNum] = acc_SSE;
  }
}


extern "C" __global__ void
PoseGradientKernel(float* d_grads,
                   const float* d_projdata,
                   const float* d_mask,
                   const float* d_vol,
                   const float* d_gradmask,
                   // vol/proj size
                   uint vNx, uint vNy, uint vNz, uint pNx, uint pNy,
                   // vol/proj spacing
                   float vdx, float vdy, float vdz,
                   float pdx, float pdy,
                   // vol/proj origin in real coordinates
                   float vOx, float vOy, float vOz,
                   float pOx, float pOy,
                   // camera to world rotation
                   float R00, float R01, float R02,
                   float R10, float R11, float R12,
                   float R20, float R21, float R22,
                   // camera to world translation
                   float Tx, float Ty, float Tz,
                   float u0, float v0,  // piercing point (real coords)
                   float f,
                   float ds) {   // line integral step size
  // get index in projection image from blockIdx and threadIdx
  uint px = blockIdx.x*blockDim.x + threadIdx.x;
  uint py = blockIdx.y*blockDim.y + threadIdx.y;
  uint blockNum = blockIdx.x*gridDim.y + blockIdx.y;
  // number the threads within same block
  uint numThreads = blockDim.x*blockDim.y*blockDim.z;
  uint threadNum = threadIdx.x*blockDim.y + threadIdx.y;

  // note that trying to allocate more might cause probs
  __shared__ __volatile__ float s_grads[10*256];

  if (threadNum == 0)           // supervisor thread
    for (uint i = 0; i < 10*256; i++)
      s_grads[i] = 0.0f;

  __syncthreads();  // sync so we don't accidentally clear s_grads after writing

  if (px >= pNx || py >= pNy) return;  // outside projection

  // set up source and pixel in camera coords
  float projxC = px*pdx + pOx - u0;
  float projyC = py*pdy + pOy - v0;
  float projzC = f;

  // convert to world coords with R(x+T)
  float sourcexW = R00*Tx + R01*Ty + R02*Tz;
  float sourceyW = R10*Tx + R11*Ty + R12*Tz;
  float sourcezW = R20*Tx + R21*Ty + R22*Tz;
  float projxW = R00*(projxC+Tx) + R01*(projyC+Ty) + R02*(projzC+Tz);
  float projyW = R10*(projxC+Tx) + R11*(projyC+Ty) + R12*(projzC+Tz);
  float projzW = R20*(projxC+Tx) + R21*(projyC+Ty) + R22*(projzC+Tz);

  // find direction of line integral
  float dx = projxW - sourcexW;
  float dy = projyW - sourceyW;
  float dz = projzW - sourcezW;
  float dist = sqrt(dx*dx+dy*dy+dz*dz);

  // normalize integral direction
  float normfactor = 1.0f/dist;
  dx *= normfactor;
  dy *= normfactor;
  dz *= normfactor;

  // Determine lmin and lmax as intersections of cylinder with line
  /*float dxcosg = xrayDist*d/sqrt(yr*yr+d*d);*/
  /*float r = 0.5f*(static_cast<float>(pNx-1))*pdx;*/
  /*float root = sqrt(dxcosg*dxcosg - (xrayDist*xrayDist - r*r));*/
  /*float lmin = dxcosg - root;*/
  /*float lmax = dxcosg + root;*/
  /*float lfactor = sqrt(projz*projz+d*d)/d;*/
  /*lmin *= lfactor;*/
  /*lmax *= lfactor;*/
  float lmin = 0;
  float lmax = dist;
  uint numsteps = (uint)((lmax-lmin)/ds)+1;

  // start at distance lmin from the source
  float x = sourcexW + lmin*dx;
  float y = sourceyW + lmin*dy;
  float z = sourcezW + lmin*dz;

  // Now convert to index coords
  dx *= ds/vdx;
  dy *= ds/vdy;
  dz *= ds/vdz;

  // convert to index
  x = (x-vOx)/vdx;
  y = (y-vOy)/vdy;
  z = (z-vOz)/vdz;

  float projVal = 0.0f;

  float delIx, delIy, delIz;
  float delIsum[3], xcrossdelIsum[3], weightdelIsum[3];
  delIsum[0] = delIsum[1] = delIsum[2] = 0;
  xcrossdelIsum[0] = xcrossdelIsum[1] = xcrossdelIsum[2] = 0;
  weightdelIsum[0] = weightdelIsum[1] = weightdelIsum[2] = 0;

  // line integral length
  float int_len = sqrt((projxC-u0)*(projxC-u0) + (projyC-v0)*(projyC-v0) + f*f);

  for (uint k = 0; k < numsteps; ++k) {
    if (x <= -1 || y <= -1 || z <= -1 ||
        x >= vNx || y >= vNy || z >= vNz) {
      // increment current position along line
      x += dx;
      y += dy;
      z += dz;
      continue;  // 0 out here, no need to Trilerp (big perf. increase)
    }
    // trilerp to get value

    float Ix = realTrilerpWithGradient(d_vol, vNx, vNy, vNz,
                                       x, y, z,
                                       delIx, delIy, delIz,
                                       0.0f);
    delIx /= vdx;
    delIy /= vdy;
    delIz /= vdz;

    // doing a raycast still
    projVal += Ix;

    // sum gradients over only middle ___ of volume
    // if ((x-vNx*.5)*(x-vNx*.5) + (y-vNy*.5)*(y-vNy*.5) <= (vNx*.2)*(vNx*.2) &&
    //     (z - vNz*.5)*(z-vNz*.5) < 64.0)

    // if (1)

    // // avoid gradients close to edges - note: this gives BAD
    // // results for the skull test data: shrinks z' if
    // if (fabs(2.0*x/vNx-1.0) < .7 && fabs(2.0*y/vNy-1.0) < .7 &&
    //     fabs(2.0*z/vNz-1.0) < .8) {

    // if (fabs(2.0*x/vNx-1.0) < .7 && fabs(2.0*y/vNy-1.0) < .7 &&
    //     fabs(2.0*z/vNz-1.0) < .8) {
    if (d_gradmask == 0 ||
        RealArrayGet3DNN(d_gradmask, vNx, vNy, vNz, x, y, z) == 1.0) {

        // sum up delI and x cross delI
        delIsum[0] += delIx;
        delIsum[1] += delIy;
        delIsum[2] += delIz;

        // need the actual world coordinates for xcrossdelIsum
        float wx = vOx + vdx * x;
        float wy = vOy + vdy * y;
        float wz = vOz + vdz * z;

        xcrossdelIsum[0] += wy*delIz - wz*delIy;
        xcrossdelIsum[1] += wz*delIx - wx*delIz;
        xcrossdelIsum[2] += wx*delIy - wy*delIx;

        float weight = k*1.0/int_len;

        weightdelIsum[0] += weight*delIx;
        weightdelIsum[1] += weight*delIy;
        weightdelIsum[2] += weight*delIz;
      }

    // increment current position along line
    x += dx;
    y += dy;
    z += dz;
  }

  // record this pixel's (thread's) gradients
#ifdef GAUSSIAN_CALIB
  float datamatch;
  if (d_mask == 0)
    datamatch = ds*projVal - d_projdata[py*pNx+px];
  else  // projval *should* be zero if mask is zero
    datamatch = ds*projVal - d_projdata[py*pNx+px]*d_mask[py*pNx+px];
  // Note: The rotation is done in the python function!
  for (int d = 0; d < 3; ++d) {
    s_grads[threadNum*10+d] = delIsum[d]*datamatch*ds;
    s_grads[threadNum*10+d+3] = xcrossdelIsum[d]*datamatch*ds;
    s_grads[threadNum*10+d+6] = weightdelIsum[d]*datamatch*ds;
  }

  // record the SSE as we go
  s_grads[threadNum*10+9] = datamatch*datamatch;  // SSE term
#else                                             // poisson instead
  if (fabs(projVal) < 1e-9) {                       // NaN check
    for (int d = 0; d < 3; ++d) {
      s_grads[threadNum*10+d] = 0;
      s_grads[threadNum*10+d+3] = 0;
      s_grads[threadNum*10+d+6] = 0;
    }
  } else {
    float datamatch = -d_projdata[py*pNx+px]/(ds*projVal)+1.0f;
    for (int d = 0; d < 3; ++d) {
      s_grads[threadNum*10+d] = delIsum[d]*datamatch*ds;
      s_grads[threadNum*10+d+3] = xcrossdelIsum[d]*datamatch*ds;
      s_grads[threadNum*10+d+6] = weightdelIsum[d]*datamatch*ds;
    }
  }
  s_grads[threadNum*10+9] = -d_projdata[py*pNx+px]*
    log(ds*projVal+1e-4) + ds*projVal;  // SSE term
#endif

  __syncthreads();

  if (threadNum == 0) {  // now accumulate this block and put into d_grads
    float acc_gradT, acc_gradR, acc_gradI;
    for (int d = 0; d < 3; ++d) {
      acc_gradT = 0;
      acc_gradR = 0;
      acc_gradI = 0;

      // reduce the block
      // TODO(crr): this can be done faster, see cudaReduce.cu
      for (uint i = 0; i < numThreads; i++) {
        acc_gradT += s_grads[i*10+d];
        acc_gradR += s_grads[i*10+d+3];
        acc_gradI += s_grads[i*10+d+6];
      }

      d_grads[blockNum*10+d] = acc_gradT;
      d_grads[blockNum*10+d+3] = acc_gradR;
      d_grads[blockNum*10+d+6] = acc_gradI;
    }
    float acc_SSE = 0.0f;
    for (uint i = 0; i < numThreads; i++) {
      acc_SSE += s_grads[i*10+9];
    }
    d_grads[blockNum*10+9] = acc_SSE;
  }
}

extern "C" __global__ void
PoseGradientKernelImages(float* d_Tx, float* d_Ty, float* d_Tz,
                         float* d_R0, float* d_R1, float* d_R2,
                         float* d_I0, float* d_I1, float* d_I2,
                         float* d_proj,
                         const float* d_projdata,
                         const float* d_mask,
                         const float* d_vol,
                         const float* d_gradmask,
                         // vol/proj size
                         uint vNx, uint vNy, uint vNz, uint pNx, uint pNy,
                         // vol/proj spacing
                         float vdx, float vdy, float vdz,
                         float pdx, float pdy,
                         // vol/proj origin in real coordinates
                         float vOx, float vOy, float vOz,
                         float pOx, float pOy,
                         // camera to world rotation
                         float R00, float R01, float R02,
                         float R10, float R11, float R12,
                         float R20, float R21, float R22,
                         // camera to world translation
                         float Tx, float Ty, float Tz,
                         float u0, float v0,  // piercing point (real coords)
                         float f,
                         float ds) {   // line integral step size
  // get index in projection image from blockIdx and threadIdx
  uint px = blockIdx.x*blockDim.x + threadIdx.x;
  uint py = blockIdx.y*blockDim.y + threadIdx.y;

  if (px >= pNx || py >= pNy) return;  // outside projection

  // set up source and pixel in camera coords
  float projxC = px*pdx + pOx - u0;
  float projyC = py*pdy + pOy - v0;
  float projzC = f;

  // convert to world coords with R(x+T)
  float sourcexW = R00*Tx + R01*Ty + R02*Tz;
  float sourceyW = R10*Tx + R11*Ty + R12*Tz;
  float sourcezW = R20*Tx + R21*Ty + R22*Tz;
  float projxW = R00*(projxC+Tx) + R01*(projyC+Ty) + R02*(projzC+Tz);
  float projyW = R10*(projxC+Tx) + R11*(projyC+Ty) + R12*(projzC+Tz);
  float projzW = R20*(projxC+Tx) + R21*(projyC+Ty) + R22*(projzC+Tz);

  // find direction of line integral
  float dx = projxW - sourcexW;
  float dy = projyW - sourceyW;
  float dz = projzW - sourcezW;
  float dist = sqrt(dx*dx+dy*dy+dz*dz);

  // normalize integral direction
  float normfactor = 1.0f/dist;
  dx *= normfactor;
  dy *= normfactor;
  dz *= normfactor;

  float lmin = 0;
  float lmax = dist;
  uint numsteps = (uint)((lmax-lmin)/ds)+1;

  // start at distance lmin from the source
  float x = sourcexW + lmin*dx;
  float y = sourceyW + lmin*dy;
  float z = sourcezW + lmin*dz;

  // Now convert to index coords
  dx *= ds/vdx;
  dy *= ds/vdy;
  dz *= ds/vdz;

  // convert to index
  x = (x-vOx)/vdx;
  y = (y-vOy)/vdy;
  z = (z-vOz)/vdz;

  float projVal = 0.0f;

  float delIx, delIy, delIz;
  float delIsum[3], xcrossdelIsum[3], weightdelIsum[3];
  delIsum[0] = delIsum[1] = delIsum[2] = 0;
  xcrossdelIsum[0] = xcrossdelIsum[1] = xcrossdelIsum[2] = 0;
  weightdelIsum[0] = weightdelIsum[1] = weightdelIsum[2] = 0;

  // line integral length
  float int_len = sqrt((projxC-u0)*(projxC-u0) + (projyC-v0)*(projyC-v0) + f*f);

  for (uint k = 0; k < numsteps; ++k) {
    if (x <= -1 || y <= -1 || z <= -1 ||
        x >= vNx || y >= vNy || z >= vNz) {
      // increment current position along line
      x += dx;
      y += dy;
      z += dz;
      continue;  // 0 out here, no need to Trilerp (big perf. increase)
    }
    // trilerp to get value

    float Ix = realTrilerpWithGradient(d_vol, vNx, vNy, vNz,
                                       x, y, z,
                                       delIx, delIy, delIz,
                                       0.0f);
    delIx /= vdx;
    delIy /= vdy;
    delIz /= vdz;


    // gradmask should be binary
    if (d_gradmask == 0 ||
        RealArrayGet3DNN(d_gradmask, vNx, vNy, vNz, x, y, z) > 0.5) {

      // doing a raycast still
      projVal += Ix;

      // sum up delI and x cross delI
      delIsum[0] += delIx;
      delIsum[1] += delIy;
      delIsum[2] += delIz;

      // need the actual world coordinates for xcrossdelIsum
      float wx = vOx + vdx * x;
      float wy = vOy + vdy * y;
      float wz = vOz + vdz * z;

      xcrossdelIsum[0] += wy*delIz - wz*delIy;
      xcrossdelIsum[1] += wz*delIx - wx*delIz;
      xcrossdelIsum[2] += wx*delIy - wy*delIx;

      float weight = k*1.0/int_len;

      weightdelIsum[0] += weight*delIx;
      weightdelIsum[1] += weight*delIy;
      weightdelIsum[2] += weight*delIz;
    }

    // increment current position along line
    x += dx;
    y += dy;
    z += dz;
  }

  d_proj[py*pNx + px] = projVal*ds;
  // d_Tx[py*pNx + px] = delIsum[0]*ds;
  // d_Ty[py*pNx + px] = delIsum[1]*ds;
  // d_Tz[py*pNx + px] = delIsum[2]*ds;
  d_R0[py*pNx + px] = xcrossdelIsum[0]*ds;
  d_R1[py*pNx + px] = xcrossdelIsum[1]*ds;
  d_R2[py*pNx + px] = xcrossdelIsum[2]*ds;
  // d_I0[py*pNx + px] = weightdelIsum[0]*ds;
  // d_I1[py*pNx + px] = weightdelIsum[1]*ds;
  // d_I2[py*pNx + px] = weightdelIsum[2]*ds;
  d_Tx[py*pNx + px] = ds*(R00*delIsum[0] + R10*delIsum[1] + R20*delIsum[2]);
  d_Ty[py*pNx + px] = ds*(R01*delIsum[0] + R11*delIsum[1] + R21*delIsum[2]);
  d_Tz[py*pNx + px] = ds*(R02*delIsum[0] + R12*delIsum[1] + R22*delIsum[2]);
  d_I0[py*pNx + px] = -ds*(R00*weightdelIsum[0] + R10*weightdelIsum[1] + R20*weightdelIsum[2]);
  d_I1[py*pNx + px] = -ds*(R01*weightdelIsum[0] + R11*weightdelIsum[1] + R21*weightdelIsum[2]);
  d_I2[py*pNx + px] = ds*(R02*weightdelIsum[0] + R12*weightdelIsum[1] + R22*weightdelIsum[2]);

}

__device__ float
imageValBG(const float* d_vol,
           uint vNx, uint vNy, uint vNz,
           int x, int y, int z) {
  // forward differences
  if (x >= 0 && x < static_cast<int>(vNx) && y >= 0 &&
      y < static_cast<int>(vNy) && z >= 0 && z < static_cast<int>(vNz))
    return RealArrayGet3D(d_vol, vNx, vNy, vNz,
                          (uint)x, (uint)y, (uint)z);
  else
    return 0.0f;
}

__device__ float
U(const float* d_vol, uint vNx, uint vNy, uint vNz,
  float vdx, float vdy, float vdz,
  int x, int y, int z, float eps) {
  // forward differences
  float Ival = imageValBG(d_vol, vNx, vNy, vNz, x, y, z);
  float Ixp = (imageValBG(d_vol, vNx, vNy, vNz, x+1, y, z) - Ival)/vdx;
  float Iyp = (imageValBG(d_vol, vNx, vNy, vNz, x, y+1, z) - Ival)/vdy;
  float Izp = (imageValBG(d_vol, vNx, vNy, vNz, x, y, z+1) - Ival)/vdz;

  float norm = sqrt(Ixp*Ixp + Iyp*Iyp + Izp*Izp);

  if (norm < eps) {
    return eps;
  } else {
    return norm;
  }
}


extern "C" __global__ void
ComputeTVDerivative(float* dU_vol,      // derivative image
                    const float* d_vol,  // image to take the derivative of
                    uint vNx, uint vNy, uint vNz,  // vol size
                    float vdx, float vdy, float vdz,  // volume spacing
                    float eps,
                    uint cbMod) {  // checkerboard (0, 1, else all)
  // Note: vdx/vdy/vdz only matter if they differ: otherwise they cancel

  // get index in projection image from blockIdx and threadIdx
  int vx = threadIdx.x;
  int vy = blockIdx.x;
  int vz = blockIdx.y;
  if (vx >= vNx || vy >= vNy || vz >= vNz) return;  // outside volume

  dU_vol[vz*vNx*vNy+vy*vNx+vx] = 2.2;

  // if cbMod is 0 or 1, do the appropriate checkerboard.  Otherwise, do all
  if ((vx+vy+vz)%2 == cbMod) {
    dU_vol[vz*vNx*vNy+vy*vNx+vx] = 0.0;
  } else {
    float u, umx, umy, umz;
    float Ixm, Iym, Izm, Ixp, Iyp, Izp, Ival;

    Ival = imageValBG(d_vol, vNx, vNy, vNz, vx, vy, vz);
    Ixp = (imageValBG(d_vol, vNx, vNy, vNz, vx+1, vy, vz) - Ival)/vdx;
    Iyp = (imageValBG(d_vol, vNx, vNy, vNz, vx, vy+1, vz) - Ival)/vdy;
    Izp = (imageValBG(d_vol, vNx, vNy, vNz, vx, vy, vz+1) - Ival)/vdz;
    Ixm = (Ival - imageValBG(d_vol, vNx, vNy, vNz, vx-1, vy, vz))/vdx;
    Iym = (Ival - imageValBG(d_vol, vNx, vNy, vNz, vx, vy-1, vz))/vdy;
    Izm = (Ival - imageValBG(d_vol, vNx, vNy, vNz, vx, vy, vz-1))/vdz;

    u = U(d_vol, vNx, vNy, vNz, vdx, vdy, vdz, vx, vy, vz, eps);
    umx = U(d_vol, vNx, vNy, vNz, vdx, vdy, vdz, vx-1, vy, vz, eps);
    umy = U(d_vol, vNx, vNy, vNz, vdx, vdy, vdz, vx, vy-1, vz, eps);
    umz = U(d_vol, vNx, vNy, vNz, vdx, vdy, vdz, vx, vy, vz-1, eps);

    dU_vol[vz*vNx*vNy+vy*vNx+vx] = Ixm/umx+Iym/umy+Izm/umz - (Ixp+Iyp+Izp)/u;
    // dU_vol[vz*vNx*vNy+vy*vNx+vx] = Ixm;
  }
}
