'''Utility functions for ConebeamProjection. Not intended to be imported by other code'''

import PyCA.Core as ca
import PyCA.Common as common


def RectFilter(Imfilt, Im, size):
    '''implements a separable box filter for an image'''

    if IsNumber(size):
        if Is3D(Im):
            size = [size, size, size]
        else:
            size = [size, size, 1]

    kernels = [[1]*sz for sz in size]
    ConvolveSeparable(Imfilt, Im, kernels)


def ConvolveSeparable(Imfilt, Im, kernels, normalize=True):
    '''does a convolution with (up to) 3 separable kernels:
    '''
    assert id(Imfilt) != id(Im)

    tmp = Im.copy()

    kx = common.ImFromNPArr(np.array(kernels[0]), Im.memType())
    ky = common.ImFromNPArr(np.array(kernels[1]), Im.memType())
    try:
        kz = common.ImFromNPArr(np.array(kernels[2]), Im.memType())
    except ValueError:
        kz = None
    if normalize:
        kx /= ca.Sum(kx)
        ky /= ca.Sum(ky)
        if kz is not None:
            kz /= ca.Sum(kz)

    ca.Convolve(Imfilt, Im, kx)
    ca.Convolve(tmp, Imfilt, ky)
    if kz is not None:
        ca.Convolve(Imfilt, tmp, kz)
    else:
        ca.Copy(Imfilt, tmp)


def SetRegionLT(I1, I2, A, B, scratchI=None):
    '''I1(I2 < A) = B

    I1, I2 are Image3D's and can be identical.
    A, B are either Image3D's or constants
    scratchI is an optional scratch Image3D
    '''

    if scratchI is None:
        scratchI = ca.Image3D(I1.grid(), I1.memType())
    ca.LT(scratchI, I2, A)
    Not_I(scratchI)
    I1 *= scratchI
    Not_I(scratchI)
    scratchI *= B
    ca.Add_I(I1, scratchI)


def Not_I(Im):
    '''performs a binary NOT of a 0 and 1 valued Image3D

    Io = !Ii ( = -Ii+1)

    '''
    ca.Neg_I(Im)
    ca.AddC_I(Im, 1.0)
